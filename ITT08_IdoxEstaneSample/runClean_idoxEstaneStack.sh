#!/bin/bash
#SBATCH -t 3:00:00
#SBATCH -N 1
#SBATCH -A uco

# Set fileName=xxx (no spaces), where the input file is pfw_input_xxx.py 

fileName=idoxEstaneStack
runningFileName=idoxEstaneStack # this sets the run location directory name
tifLocation='__TIF_DIRECTORY_PATH__'
runLocation='__PATH_TO_RUNLOCATION__'
userBank="__USER_BANK__"
userName="$(whoami)"
WRITE_RUN_INFO=false  # Should run info of successful run be appended to readme

# ==========================================================================================================================================
# This should be the location of the input file and anything else you need to copy over:
fileLocation='__PATH_TO_pfw_input_baselineCylinderTest.py__'
pfwFileLocation='__PATH_TO_PFW_AND_GEOMCLASSES__'
geosPath='__PATH_TO_GEOS__'

# Function to retrieve Git data
get_git_info() {
        cd "$1" || exit 1
        git_url=$(git config --get remote.origin.url)
        git_branch=$(git rev-parse --abbrev-ref HEAD)
        git_commit=$(git rev-parse HEAD)
        cd - >/dev/null || exit 1
}

# Write runtime data to workflow readme
if [ "$WRITE_RUN_INFO" = true ]; then
        # Find the last occurrence of /GEOS/ in geosPath
        geos_index=$(expr "$geosPath" : '.*/GEOS/')
        geosPathShort="${geosPath:0:geos_index-1}"

        echo "$geosPathShort"

        # Get current date
        current_datetime=$(date +"%Y-%m-%d %H:%M:%S %Z")

        # Get Git info for geos 
        get_git_info "$geosPathShort"
        geos_git_info="\n    GEOS Git Info:\n    - URL: $git_url\n    - Branch: $git_branch\n    - Commit: $git_commit"

        # Get Git info for pfw
        get_git_info "$pfwFileLocation"
        pfw_git_info="\n    PFW Git Info:\n    - URL: $git_url\n    - Branch: $git_branch\n    - Commit: $git_commit"

        bash_version=$(bash --version | head -n 1)
        # Append Git info to README.md in inputDirectory
        readme_file="$fileLocation/README.md"

        # Remove existing Git info from README
        sed -i '/^================================================================================/,/^================================================================================/d' "$readme_file"

        last_line=$(grep -n -v "^\s*$" "$readme_file" | cut -d: -f1 | tail -n 1)
        sed -i "${last_line}a \\ \\
================================================================================ \\
\\
### Run Date and time: $current_datetime \\
### Bash Version: $bash_version \\
        $geos_git_info \\
        $pfw_git_info \\
================================================================================
        " "$readme_file"
fi
if [ -n "$fileName" ]
then
        echo "Running job: "$runningFileName
        rm -rf $runLocation/$runningFileName/                                                                                   # delete old results for the same fileName!!!
        mkdir -p $runLocation/$runningFileName/                                                                         # create the run/output directory

        cp -r $tifLocation $runLocation/$runningFileName                                                                        # copy the ct Data
        cp $fileLocation/pfw_input_$fileName.py $runLocation/$runningFileName           # copy the input file
        cp $pfwFileLocation/particleFileWriter.py $runLocation/$runningFileName         # copy the preprocessor
        cp $pfwFileLocation/pfw_check.py $runLocation/$runningFileName                  # copy the autoRestart script
        cp $pfwFileLocation/pfw_geometryObjects.py $runLocation/$runningFileName        # copy the geometry object functions
        cp -r $pfwFileLocation/thirdPartyLibraries/ $runLocation/$runningFileName       # move tpls to run location

        cd $runLocation/$runningFileName  
        sed -i "s#__USERNAME__#${userName}#g" pfw_input_$fileName.py                # swap in username 
        sed -i "s#__PATH_TO_GEOS__#${geosPath}#g" pfw_input_$fileName.py            # swap in geosPath
        sed -i "s#__USER_BANK__#${userBank}#g" pfw_input_$fileName.py                                                                      # move to the run location
        srun -n36 python3 particleFileWriter.py pfw_input_$fileName                                             # launch the preprocessor
fi

