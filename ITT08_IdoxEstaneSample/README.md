# GEOS(X) Simulation(s): Dynamic simulations of Idox crystals within Estane binder with Plasticizer

*Note:*  This README is far far far behind

To run this simulation, required branch versions should be noted.
I am currently running with this workflow using my branch of the particleFileWriter, 
- feature/appleton/stlObject -- necessary for stl defined initial configurations,
and the most up-to-date branch of GEOS, 
- feature/crook5/mpm
Specifically it should be noted that other versions of GEOS do not function without 
separating the FTable input into a separate file.  By modifying the resultant 
- mpm_idoxParticle_1_1.xml 
file, the simulation will regain functionality, but that seems unnecessary all things
considered.

This simulation establishes a GEOS(X) link between a SegmentFlow generated stl
and a GEOS(X)-MPM simulation by reading the stl via the stlObject geometry object. 

Questions can be directed toward Jay Appleton (jay.appleton@colorado.edu).

## The Quick How To is wrong:

The the simulation is intended to be defined by a particle file writer input script with key values:
- thisWeibullSeed: this is a random seed used in the weibull strength scaling.  
	It is an integer value greater than or equal to zero.  Note that setting this to zero turns the weibull strength scaling off. ;
- thisWeibullModulus: a dimensionless parameter that describes the variability in measured strengths.;
- thisWeibullVolume: best guess approximate of volume of samples used to identify thisWeibullModulus. ;
- thisRefine: thisRefine is a counting number, i.e. integer > 0, that is used to refine the discretization by cutting 		partition length in half.  Note that the discretization is a uniform grid with (2\*\*thisRefine)\*thisCPP elements/		direction. ;
- thisCPP: simply the number of discretization cells per partition per direction.;
- and a few more ...

The simulation is initialized by running the particle file writer (not included).
Fill in values in runClean and userDefs.
 
================================================================================ 

### Run Date and time: 2024-02-21 18:38:33 PST 
### Bash Version: GNU bash, version 4.4.20(1)-release (x86_64-redhat-linux-gnu) 
        
    GEOS Git Info:
    - URL: https://github.com/GEOS-DEV/GEOS.git
    - Branch: feature/appleton/psaap-mpm
    - Commit: 20ae7db3235c8d32634f4eac9f2d95c1ac9981cb 
        
    PFW Git Info:
    - URL: ssh://git@czgitlab.llnl.gov:7999/homel1/pfw_geosx_temp.git
    - Branch: feature/appleton/psaap-mpm
    - Commit: 2e610daec60b3e7dab9f50b96eb62e335f60644d 
================================================================================
