# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import os
import logging
import time
import sys
import numpy as np
from skimage import io
import subprocess


# globally scoped values ------------------------------------------------------#

# define impact (1) or cosine ramped (2) loading profile
loading_option = 2
max_velocity = 3.0/1000  # m/s converted to mm/us
target_displacement = 0.625 # mm
displacement_end_time = 550
simulation_end_time = 350
total_frame_count = 350

# default values:

default_weibull_seed = 1
default_weibull_volume = 1.0e-5      # __WEIBULL_VOLUME__ Picked to give good results

class Sapphire:
    # elastic isotropic
    density = 1.0
    youngs_modulus = 1.0
    poisson_ratio = 1.0
    bulk_modulus = 1.0
    shear_modulus = 1.0
    material_number = 0

class Idox:
    # ceramic damage
    density = __IDOX_DENSITY__ # 2.001                                          # Idox density
    youngs_modulus = __IDOX_YOUNGS_MODULUS__ #16.75                                   # 22.0  # GPa
    poisson_ratio = __IDOX_POISSONS_RATIO__ # 0.425                                    #0.25
    weibull_modulus = __IDOX_WEIBULL_MODULUS__ #4.5                                    # __WEIBULL_MODULUS__
    weibull_seed = default_weibull_seed
    weibull_volume = 1.0e-5
    tensile_strength = __IDOX_TENSILE_STRENGTH__ # 0.113                                 # idox GPA
    compressive_strength_scale = __IDOX_COMPRESSIVE_STRENGTH_SCALE__ # 8.0                         # Mike's 8ish-to-1 rule of thumb
    bulk_modulus = youngs_modulus / ( 3 * ( 1 - 2 * poisson_ratio ) )    # idox bulk
    shear_modulus = youngs_modulus / ( 2 * ( 1 + poisson_ratio ) )       # idox shear
    compressive_strength = compressive_strength_scale * tensile_strength # GPA    
    material_number = 1



# loadingOption="2"    # 1 is impact, 2 is ramped

# estaneMDensity="1.935"                            # estane density
# estaneMYoungsModulus="0.250" # GPa
# estaneMPoissonRatio="0.4"
# estaneMWeibullModulus="6.0"
# estaneMTensileStrength="0.2"             # estane GPA
# estaneMCompressiveStrengthScale="8.0"  # Mike's 8ish-to-1 rule of thu
class EstaneMatrix:
    # ceramic damage
    density =  __EM_DENSITY__       # 1.935    # estane density 
    youngs_modulus = __EM_YOUNGS_MODULUS__     #   0.250 # GPa
    poisson_ratio = __EM_POISSONS_RATIO__       # 0.4
    weibull_modulus = __EM_WEIBULL_MODULUS__   # 6.0
    tensile_strength = __EM_TENSILE_STRENGTH__ # 0.2 # estane GPA
    compressive_strength_scale = __EM_COMPRESSIVE_STRENGTH_SCALE__     # 8.0  # Mike's 8ish-to-1 rule of thumb
    bulk_modulus = youngs_modulus / ( 3 * ( 1 - 2 * poisson_ratio ) )    # estane bulk
    shear_modulus = youngs_modulus / ( 2 * ( 1 + poisson_ratio ) )       # estane shear
    compressive_strength = compressive_strength_scale * tensile_strength # GPA
    material_number = 2    


def trim_zeroes(arr):
    # Find non-zero indices
    non_zero_indices = np.nonzero(arr)
    
    # Determine the bounding box of non-zero values
    x_min, x_max = min(non_zero_indices[0]), max(non_zero_indices[0])
    y_min, y_max = min(non_zero_indices[1]), max(non_zero_indices[1])
    z_min, z_max = min(non_zero_indices[2]), max(non_zero_indices[2])
    
    # Slice the array to keep only the non-zero parts
    trimmed_arr = arr[x_min:x_max+1, y_min:y_max+1, z_min:z_max+1]
    
    return trimmed_arr

def trim_zeroes_with_indexes( arr, cutoff_value: int = 0 ):
    # Find non-zero indices
    trimmed_arr = arr
    arr[ arr <= cutoff_value ] = 0  # remove awkward padding
    non_zero_indices = np.nonzero(arr)
    
    # Determine the bounding box of non-zero values
    x_min, x_max = min(non_zero_indices[0]), max(non_zero_indices[0])
    y_min, y_max = min(non_zero_indices[1]), max(non_zero_indices[1])
    z_min, z_max = min(non_zero_indices[2]), max(non_zero_indices[2])
    
    # Slice the array to keep only the non-zero parts
    trimmed_arr = trimmed_arr[x_min:x_max+1, y_min:y_max+1, z_min:z_max+1]
    
    return trimmed_arr, (x_min, y_min, z_min, x_max, y_max, z_max)

def time_formatter(time):
    hours = int( time // 3600 )
    minutes = int( ( time % 3600 ) // 60 )
    seconds = int( time % 60 )
    
    # Format runtime in hours:minutes:seconds
    return f"{hours:02}:{minutes:02}:{seconds:02}"

# Count instances of grain_id (integer) in array (numpy array)
def count_id_voxels(core_id, array, grain_id):
    array_copy = np.copy(array)
    array_copy[ array != grain_id ] = 0

    # trim array copy in preparation for writing geometry object
    trimmed_array, bounding_box = trim_zeroes_with_indexes(array_copy)

    nonzero_count = np.count_nonzero(array_copy)
    nonzero_count_2 = np.count_nonzero( trimmed_array )
    if nonzero_count != nonzero_count_2:
        logFormatter(" trimmed array not equal to original array! ", "ERROR" )
        sys.exit(1)

    print(f"        Core: {core_id}; Grain: {grain_id}; Size: {nonzero_count}")
    print(f"        Trimmed array box: {bounding_box} ")
    return nonzero_count

# helper script to process idox crystals:
def process_grain_stack( ct_grains, ct_min_x, ct_min_y, ct_min_z, ct_max_x, 
                  ct_max_y, ct_max_z, dx, dy, dz, this_flaw_size, vmin, 
                  lift_z=0, class_name=Idox):

    # ct_grains is a numpy array with crystal integers ranging from 1 to num_grains
    # we will use the new grain_stack object that use the integer flags to define 
    # surfaces.

    weibull_volume = class_name.weibull_volume 
    weibull_modulus = class_name.weibull_modulus 
    weibull_seed = class_name.weibull_seed

    temp_array = np.copy( ct_grains )
    # Get the dimensions of the temp_array
    nx, ny, nz = np.shape(temp_array)

    # Calculate the step sizes in each direction
    step_x = ( ct_max_x - ct_min_x ) / ( nx - 1 )
    step_y = ( ct_max_y - ct_min_y ) / ( ny - 1 )
    step_z = ( ct_max_z - ct_min_z ) / ( nz - 1 )

    temp_array_crop, (x_min, y_min, z_min, x_max, y_max, z_max) = trim_zeroes_with_indexes(temp_array, 1)
    nnx, nny, nnz = np.shape( temp_array_crop )
    # logFormatter( f"Grain {grain_id} has been trimmed from [{nx}, {ny}, {nz}] to [{nnx}, {nny}, {nnz}]")

    crop_bounds_min_x = ct_min_x + step_x * x_min
    crop_bounds_min_y = ct_min_y + step_y * y_min
    crop_bounds_min_z = ct_min_z + step_z * z_min

    crop_bounds_max_x = ct_min_x + step_x * x_max 
    crop_bounds_max_y = ct_min_y + step_y * y_max 
    crop_bounds_max_z = ct_min_z + step_z * z_max 
    
    grain_object = geom.GrainStack("grain_stack", temp_array_crop,  
        [ crop_bounds_min_x, crop_bounds_min_y, crop_bounds_min_z + lift_z - ct_min_z ], 
        [ crop_bounds_max_x, crop_bounds_max_y, crop_bounds_max_z + lift_z - ct_min_z ], 
        mpm_delta=[dx, dy, dz], v=[0.0, 0.0, 0.0], mat=class_name.material_number, group=0 )
    
    if weibull_seed == 0:
        # unmodified grain_object is requested

        return grain_object
    else:
        # voronoi cell based strength scaled grain_object is requested

        padding = 3.0 * np.max( [ dx, dy, dz ] )

        vw_grain_object = geom.voronoiWeibullBoxWrapper(name=f"vw_grain_stack", subObject=grain_object,
            x0=[ crop_bounds_min_x - padding, crop_bounds_min_y - padding, crop_bounds_min_z + lift_z - ct_min_z - padding ],
            x1=[ crop_bounds_max_x + padding, crop_bounds_max_y + padding, crop_bounds_max_z + lift_z - ct_min_z + padding ],
            flawSize=this_flaw_size, weibullVolume=weibull_volume, weibullModulus=class_name.weibull_modulus, 
            weibullSeed=weibull_seed, vMin=vmin)
        
        return vw_grain_object



# CT Class contains information and functions required to handle the CT data
class CTScanner:
    tif_directory = 'Sample_TifStack/'
    scaling_factor = 1.0 / 1000.0
    ct_voxelSize = 6.08 * scaling_factor # um to mm  voxel edge length
    domain_scale = 1.2    # how much larger should the domain be than the ct object

    def __init__(self):

        # (begin) variable scoped locally to __init__
        files = [file for file in os.listdir(self.tif_directory) if file.endswith('.tif')]
        file = files[0]
        full_path = self.tif_directory + file
        tif_layer = io.imread(full_path, as_gray=True)
        array_size = tif_layer.shape

        num_part = file.split('.')[0]
        num_digits = len(num_part)

        # (begin) variable scoped locally to __init__
        
        self.ct_ni, self.ct_nj = array_size
        self.ct_nk =  len(files)

        self.lenx = ( self.ct_ni - 1 ) * self.ct_voxelSize
        self.leny = ( self.ct_nj - 1 ) * self.ct_voxelSize
        self.lenz = ( self.ct_nk - 1 ) * self.ct_voxelSize  # this is not correct?

        self.ct_min_x =  -0.5 * self.lenx
        self.ct_max_x =   0.5 * self.lenx
        self.ct_min_y =  -0.5 * self.leny
        self.ct_max_y =   0.5 * self.leny

        self.ct_min_z =  0.0
        self.ct_max_z = self.lenz

        self.max_domain_length = self.domain_scale * np.max([self.lenx, self.leny, self.lenz]) 

        read_tif_timer_start = time.time()
        self.read_tif_images( num_digits )
        read_tif_timer_end = time.time()

        self.ct_read_time = read_tif_timer_end - read_tif_timer_start

    def read_tif_images( self, num_digits ):
        """
        Read the TIF images in serial and store them into numpy arrays.
        """     

        self.ct_idox = np.empty((self.ct_ni, self.ct_nj, self.ct_nk), dtype=np.uint16)
        self.ct_binder = np.empty((self.ct_ni, self.ct_nj, self.ct_nk), dtype=np.uint16)

        for file_cnt in range(self.ct_nk):
            file = f"{file_cnt:0{num_digits}d}.tif"
            full_path = os.path.join(self.tif_directory, file)
            tif_layer = io.imread(full_path, as_gray=True)

            self.ct_idox[:, :, file_cnt] = tif_layer
            self.ct_binder[:, :, file_cnt] = tif_layer

        self.ct_idox[ np.abs( self.ct_idox - 1 ) < 1e-14 ] = 0
        self.ct_idox[ self.ct_idox > 1 ] = self.ct_idox[ self.ct_idox > 1 ] - 1
        self.ct_binder[ self.ct_binder != 0 ] = 1
        self.ct_num_crystals = np.max( self.ct_idox )

# Function to initialize discretization based on CTScanner data
def initialize_discretization(ct_scanner):
    return {
        'partitions_per_direction': __THIS_PPD__, # 25,  # 25 is the max to consider i think
        'cells_per_partition': __THIS_CPP__, # 12,
        'particles_per_cell': 2,
        'max_domain_length': ct_scanner.max_domain_length,
        'ct_lenx': ct_scanner.lenx,
        'ct_leny': ct_scanner.leny,
        'ct_lenz': ct_scanner.lenz,
        'ct_min_x': ct_scanner.ct_min_x,
        'ct_min_y': ct_scanner.ct_min_y,
        'ct_min_z': ct_scanner.ct_min_z,
        'ct_max_x': ct_scanner.ct_max_x,
        'ct_max_y': ct_scanner.ct_max_y,
        'ct_max_z': ct_scanner.ct_max_z,
    }

class GeometryObjects:
    ppvc = 10  
        # Particles per cell
    ppc = 2                       # particles per cell in each direction

    def __init__( self, comm, rank, num_ranks ):
        # #--- Define Domain --------------------------------------------------#

        # self.ct_idox = None 
        # self.ct_binder = None 
        # self.discretization = None 
        self.objects = []
        # self.accumulated_objects = []
        # self.ct_scaling_factor = 0.0
        # self.ct_num_crystals = None 

        # Initialize CTScanner and discretization inside the class
        # if rank == 0:
        self.ct_scanner = CTScanner()  # Instantiate the CTScanner class
        self.ct_num_crystals = self.ct_scanner.ct_num_crystals
        self.ct_idox = self.ct_scanner.ct_idox
        self.ct_binder = self.ct_scanner.ct_binder
        self.ct_binder[ self.ct_binder != 0 ] = 1
        self.ct_scaling_factor = self.ct_scanner.scaling_factor
        # define discretization dictionary on ct_scanner data
        self.discretization = initialize_discretization(self.ct_scanner)  

        # if num_ranks > 1:
        #     self.ct_idox = comm.bcast(self.ct_idox, root=0)
        #     self.ct_binder = comm.bcast(self.ct_binder, root=0)
        #     self.discretization = comm.bcast(self.discretization, root=0)
        #     self.ct_scaling_factor = comm.bcast(self.ct_scaling_factor, root=0) 
        #     self.ct_num_crystals = comm.bcast(self.ct_num_crystals, root=0) 

        # Adding the new attributes for domain boundaries
        self.xmin = -self.discretization['max_domain_length'] / 2.0        # mm
        self.xmax = self.discretization['max_domain_length'] / 2.0         # mm
        self.ymin = -self.discretization['max_domain_length'] / 2.0        # mm
        self.ymax = self.discretization['max_domain_length'] / 2.0         # mm
        self.zmin = 0.0                             # mm
        self.zmax = self.discretization['max_domain_length']              # mm

        # Discretization keys, use values from the discretization dictionary
        self.xpar = 1.0 * self.discretization['partitions_per_direction']
        self.ypar = 1.0 * self.discretization['partitions_per_direction']
        self.zpar = 1.0 * self.discretization['partitions_per_direction']

        # Use xpar, ypar, and zpar to calculate nI, nJ, nK
        self.nI = 2.0 + self.xpar * self.discretization['cells_per_partition']
        self.nJ = 2.0 + self.ypar * self.discretization['cells_per_partition']
        self.nK = self.zpar * self.discretization['cells_per_partition']

        self.dx = ( self.xmax - self.xmin ) / ( self.nI - 2) / self.ppc
        self.dy = ( self.ymax - self.ymin ) / ( self.nJ - 2) / self.ppc
        self.dz = ( self.zmax - self.zmin ) / ( self.nK - 2) / self.ppc

        self.this_flaw_size = np.min( [ self.dx, self.dy, self.dz ] ) * self.ppvc     
        self.this_v_min = self.dx * self.dy * self.dz


        platen_depth = ( self.discretization['max_domain_length'] - self.discretization['ct_lenz'] ) / 2.0
        self.top_platen_depth = 1.25 * platen_depth
        self.base_platen_depth = 0.75 * platen_depth

        # if rank == 0:
        # define platens and binder
        base_platen = geom.box('base', [-100, -100, -100], [100, 100, self.base_platen_depth], 
            v=[0,0,0], mat=Sapphire.material_number, group=0)

        top_platen = geom.box('top', [-100, -100, self.zmax - self.top_platen_depth ], [100, 100, 100], 
            v=[0,0,0], mat=Sapphire.material_number, group=0)

        binder_stack = geom.ctScene('binder', self.ct_binder,  
            [self.discretization['ct_min_x'], self.discretization['ct_min_y'], self.base_platen_depth - self.discretization['ct_min_z']],  
            [self.discretization['ct_max_x'], self.discretization['ct_max_y'], self.zmax - self.top_platen_depth ],
            mpm_delta=[self.dx,self.dy,self.dz], v=[0.0, 0.0, 0.0], mat=EstaneMatrix.material_number, group=0 )

        self.objects += [ base_platen, top_platen, binder_stack ]

        # if rank != 0 or num_ranks == 1:
        # divide and conquer individual crystals

        available_ranks = num_ranks - 1 if num_ranks > 1 else 1
        num_crystals = self.ct_num_crystals

        crystals_per_rank_base = num_crystals // num_ranks  # floor division
        extra_crystals = num_crystals % num_ranks           # left overs go to one per core until filled

        if rank >= extra_crystals:
            start_index = rank * crystals_per_rank_base + extra_crystals  # all crystals for lower ranks and all extra crystals
            end_index = start_index + crystals_per_rank_base - 1
        else:
            start_index = rank * crystals_per_rank_base + rank   # + rank, because each lower rank gets an extra crystal
            end_index = start_index + crystals_per_rank_base

        # # # helper script to process idox crystals:
        # def process_grain_stack( ct_grains, 
        #                   ct_min_x, ct_min_y, ct_min_z, ct_max_x, 
        #                   ct_max_y, ct_max_z, 
        #                   dx, dy, dz, this_flaw_size, vmin,  
        #                   lift_z=0, class_name=Idox):
        crystal_stack = process_grain_stack( self.ct_idox,
            self.discretization[ 'ct_min_x' ], self.discretization[ 'ct_min_y' ], self.discretization[ 'ct_min_z' ],
            self.discretization[ 'ct_max_x' ], self.discretization[ 'ct_max_y' ], self.discretization[ 'ct_max_z' ],
            self.dx, self.dy, self.dz, self.this_flaw_size, self.this_v_min, 
            lift_z=self.base_platen_depth, class_name=Idox ) 

        self.objects.insert(0, crystal_stack)

        # logFormatter(f"(END) Defining Idox Objects.")

        # pfw["objects"] = objects + [top_platen, base_platen, Binder] 
        # numObjects = len(pfw["objects"])
        # logFormatter( f"number of objects in pfw object list: {numObjects} ")

class PFW:

    # item_list = []

    # item_list.append( ['runDebug', 'mBatch', 'mWalltime', 'mSubmitJobs', 'autoRestart', 'mBank'] )

    # Solver Values ------------------------------------------------------------
    endTime = simulation_end_time  # 320
    updateMethod = "XPIC"
    updateOrder = 2
    useAPIC = 0
    useInternalForceAsFaceReaction = 1
    timeIntegrationOption = "ExplicitDynamic"
    cflFactor = 0.25
    initialDt = 1e-16
    cpdiDomainScaling = 1
    bodyForce = [ 0, 0, 0 ]
    boundaryConditionTypes = [ 0, 0, 0, 0, 2, 2 ]
    prescribedBcTable = 0
    prescribedBoundaryFTable = 1
    fTableInterpType = "Cosine"
    damageFieldPartitioning = 1
    needsNeighborList = 1
    # useDamageAsSurfaceFlag = 1
    frictionCoefficient = 0.02
    materials = [ "Sapphire", "Idox", "EstaneMatrix" ]

    # item_list.append( ['endTime', 'updateMethod', 'updateOrder', 'useAPIC', 
    #     'useInternalForceAsFaceReaction', 'timeIntegrationOption', 'cflFactor',
    #     'initialDt', 'cpdiDomainScaling', 'bodyForce', ])

    materialPropertyString =f"""
    <ElasticIsotropic
        name="Sapphire"
        defaultDensity="3.5"
        defaultBulkModulus="315"
        defaultShearModulus="172"
        />
    <CeramicDamage
        name="Idox"
        defaultDensity="{Idox.density}"
        defaultBulkModulus="{Idox.bulk_modulus}"
        defaultShearModulus="{Idox.shear_modulus}"
        tensileStrength="{Idox.tensile_strength}"
        compressiveStrength="{Idox.compressive_strength}"
        maximumStrength="{1.25 * Idox.compressive_strength}"
        crackSpeed="1.8"
        />
    <CeramicDamage
        name="EstaneMatrix"
        defaultDensity="{EstaneMatrix.density}"
        defaultBulkModulus="{EstaneMatrix.bulk_modulus}"
        defaultShearModulus="{EstaneMatrix.shear_modulus}"
        tensileStrength="{EstaneMatrix.tensile_strength}"
        compressiveStrength="{EstaneMatrix.compressive_strength}"
        maximumStrength="{1.25 * EstaneMatrix.compressive_strength}"
        crackSpeed="2.5"
        />
    """

    def __init__(self, comm, rank, num_ranks):
        self.geometry_objects = GeometryObjects( comm, rank, num_ranks )  # Instantiate the CTScanner class

        # Batch submission keys:
        self.runDebug = False
        self.mBatch = True
        self.mWallTime = "15:30:00"   # "15:30:00"
        # self.mWallTime = "00:59:00"   # "15:30:00"
        self.mSubmitJobs = True
        self.autoRestart = True
        self.mBank = "uco"

        self.ppc = self.geometry_objects.ppc
        self.discretization = self.geometry_objects.discretization
        self.ct_scaling_factor = self.geometry_objects.ct_scaling_factor
        self.objects = self.geometry_objects.objects

        # Discretization keys, use values from the discretization dictionary
        self.xpar = np.int(self.discretization['partitions_per_direction'])
        self.ypar = np.int(self.discretization['partitions_per_direction'])
        self.zpar = np.int(self.discretization['partitions_per_direction'])

        # Use xpar, ypar, and zpar to calculate nI, nJ, nK
        self.nI = np.int( 2 + self.xpar * self.discretization['cells_per_partition'] )
        self.nJ = np.int( 2 + self.ypar * self.discretization['cells_per_partition'] )
        self.nK = np.int( self.zpar * self.discretization['cells_per_partition'] )

        # Adding the new attributes for domain boundaries
        self.xmin = self.geometry_objects.xmin              # mm
        self.xmax = self.geometry_objects.xmax              # mm
        self.ymin = self.geometry_objects.ymin              # mm
        self.ymax = self.geometry_objects.ymax              # mm
        self.zmin = self.geometry_objects.zmin              # mm
        self.zmax = self.geometry_objects.zmax              # mm

        self.dx = ( self.xmax - self.xmin ) / ( self.nI - 2) / self.ppc
        self.dy = ( self.ymax - self.ymin ) / ( self.nJ - 2) / self.ppc
        self.dz = ( self.zmax - self.zmin ) / ( self.nK - 2) / self.ppc
        
        self.plotInterval = self.endTime / total_frame_count
        self.restartInterval = self.endTime / 60
        self.reactionHistory = 1
        self.reactionWriteInterval = self.endTime / 7500.0
        self.boxAverageHistory = 0

        # FTable:
        # Dynamice "Striker" loading, see Idox-estane 1a
        target_domain_strain = target_displacement / self.discretization['max_domain_length']
        striker_time = ( target_domain_strain * self.discretization['max_domain_length'] * np.pi )/(2.0 * max_velocity)
        target_relax_displacement = 2.0 * target_displacement # applied over a long time
        this_end_stretch = 1 - ( target_displacement / self.discretization['max_domain_length'] )
        this_end_relax = this_end_stretch + ( target_relax_displacement / self.discretization['max_domain_length'] )
        end_ex_time = 5.0 * striker_time

        if loading_option == 1:
            self.fTable = [[-striker_time, 1, 1, 2 - this_end_stretch],
            [striker_time, 1, 1, this_end_stretch],
            [end_ex_time, 1, 1, this_end_relax]]
        elif loading_option == 2:
            self.fTable = [[0, 1, 1, 1],
            [striker_time, 1, 1, this_end_stretch],
            [end_ex_time, 1, 1, this_end_relax]]

        # self.particleFileFields = [ 'Velocity', 'MaterialType', 'ContactGroup', 
        #                             'SurfaceFlag', 'RVector', 'StrengthScale', 'Damage' ]
        self.plottableFields = [ 'particleID', 'particleReferencePosition', 
                                 'particleDamage', 'particleCenter', 
                                 'particleMaterialType', 'particleStrengthScale',
                                 'particleSurfaceFlag', 'particleVolume' ]
        # Process values:
        #----------------------------------------------------------------------#
        self.mCores = self.xpar * self.ypar * self.zpar
        self.mNodes = int( np.ceil( float( self.mCores ) / 112. ) ) 
        #----------------------------------------------------------------------#