# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
import sys
from sklearn.neighbors import KDTree          # nearest neighbor search with KDTree
from skimage import io
import os
import subprocess
import concurrent.futures

pfw = {} 
pfw["runDebug"] = False

# Batch parameters for GEOS runs.  --------------------------------------------#
pfw["mBatch"]=True
pfw["mWallTime"]="06:30:00"
pfw["mSubmitJobs"] = True
pfw["autoRestart"] = True
pfw["mBank"]="uco"

# Values from the simulation input parser:
domainScale = __DOMAIN_SCALING_FACTOR__

# loadingOption = 1 # unsmoothed cosine from -strikerTime
# loadingOption = 2 # smoothed cosine from -strikerTime
loadingOption = __LOADING_OPTION__ # let 2 be the default

partitionsPerDirection = __THIS_PPD__ #3
cellsPerPartition = __THIS_CPP__ #12

# # Estane Matrix
estaneMDensity = __EM_DENSITY__ # 1.935				# estane density
estaneMYoungsModulus = __EM_YOUNGS_MODULUS__   # 0.250 # GPa
estaneMPoissonRatio = __EM_POISSONS_RATIO__    # 0.4
estaneMWeibullModulus = __EM_WEIBULL_MODULUS__ # 6.0
estaneMTensileStrength = __EM_TENSILE_STRENGTH__ # 0.2		# estane GPA
estaneMCompressiveStrengthScale = __EM_COMPRESSIVE_MULTIPLE__ # 8.0  # Mike's 8ish-to-1 rule of thumb

# CT info from getCTInfo.py 
#------------------------------------------------------------------------------#
tifDirectory = 'Sample_TifStack/' # this directory is copied from CtDirectory into 
                              # root at runlocation so the directory name is all
                              # that matters
scalingFactor = 1/1000    # ct info in um simulation in mm

# helper script to trim unwanted padding from crystal tif stack
# Function to trim off zeroes from the 3D array
def trim_zeroes(arr):
    # Find non-zero indices
    non_zero_indices = np.nonzero(arr)
    
    # Determine the bounding box of non-zero values
    x_min, x_max = min(non_zero_indices[0]), max(non_zero_indices[0])
    y_min, y_max = min(non_zero_indices[1]), max(non_zero_indices[1])
    z_min, z_max = min(non_zero_indices[2]), max(non_zero_indices[2])
    
    # Slice the array to keep only the non-zero parts
    trimmed_arr = arr[x_min:x_max+1, y_min:y_max+1, z_min:z_max+1]
    
    return trimmed_arr

# helper script to process idox crystals:
def process_grain(grainID, ctGrains, ct_minX, ct_minY, basePlatenDepth, ct_maxX,
                  ct_maxY, ct_maxZ, dx, dy, dz, idoxTensileStrength, thisWeibullSeed, 
                  thisWeibullVolume, idoxWeibullModulus, thisFlawSize, thisVMin):
    tempGrain = 0.0 * ctGrains
    tempGrain[ctGrains == grainID] = 1.0
    
    grain_object = geom.ctScene(f"grain_{grainID}", tempGrain,  
        [ct_minX, ct_minY, basePlatenDepth - ct_minZ], 
        [ct_maxX, ct_maxY, ct_maxZ + basePlatenDepth - ct_minZ], 
        mpm_delta=[dx, dy, dz], v=[0.0, 0.0, 0.0], mat=0, group=0 )
    
    tensileStrength = idoxTensileStrength
    if thisWeibullSeed == 0:
        # Handle specific case
        idoxTensileStrength = idoxTensileStrength  # Example placeholder
        idoxCompressiveStrength = idoxCompressiveStrengthScale * idoxTensileStrength
        return grain_object
    else:
        # Handle the other case
        idoxCompressiveStrength = idoxCompressiveStrengthScale * idoxTensileStrength
        
        vw_grain_object = geom.voronoiWeibullBoxWrapper(name=f"vw_{grainID}", subObject=grain_object,
            x0=[ct_minX, ct_minY, basePlatenDepth - ct_minZ], 
            x1=[ct_maxX, ct_maxY, (ct_nk * ct_voxelSize) + basePlatenDepth - ct_minZ], 
            flawSize=thisFlawSize, weibullVolume=thisWeibullVolume, weibullModulus=idoxWeibullModulus, 
            weibullSeed=thisWeibullSeed, vMin=thisVMin)
        
        return vw_grain_object


files = [file for file in os.listdir(tifDirectory) if file.endswith('.tif')]
file = files[0]
fullpath = tifDirectory + file
tifLayer = io.imread(fullpath, as_gray=True)
array_size = tifLayer.shape

numPart = file.split('.')[0]
numDigits = len(numPart)

ct_nk =  len(files)
ct_voxelSize = __PIXEL_SIZE__ * scalingFactor # mm # original pixelsize was 1.09um segmentflow uses others

ct_ni =   array_size[0]
ct_nj =   array_size[1]

ctScene = np.empty((ct_ni, ct_nj, ct_nk), dtype=np.uint16)
binderScene = np.empty((ct_ni, ct_nj, ct_nk), dtype=np.uint16)

for fileCnt in range(ct_nk):
	file = f"{fileCnt:0{numDigits}d}.tif"
	fullpath = tifDirectory + file
	tifLayer = io.imread(fullpath, as_gray=True)

	ctScene[:, :, fileCnt] = tifLayer
	binderScene[:, :, fileCnt] = tifLayer

# trim off zeroes in x, y, and z to make a tight fit
print(ctScene.shape)
ctScene = trim_zeroes(ctScene)
print(ctScene.shape)

ct_ni, ct_nj, ct_nk = ctScene.shape

lenx = ( ct_ni - 1 ) * ct_voxelSize
leny = ( ct_nj - 1 ) * ct_voxelSize
lenz = ( ct_nk - 1 ) * ct_voxelSize  # this is not correct?

maxDomainLength = domainScale * np.max([lenx, leny, lenz])

ct_minX =  -0.5 * ct_ni * ct_voxelSize
ct_maxX =   0.5 * ct_ni * ct_voxelSize
ct_minY =  -0.5 * ct_nj * ct_voxelSize
ct_maxY =   0.5 * ct_nj * ct_voxelSize

ct_xCenter = 0.0
ct_yCenter = 0.0

ct_minZ =  0.0
ct_maxZ = lenz

platenDepth = (maxDomainLength - lenz)/2.0
topPlatenDepth = 1.25 * platenDepth
basePlatenDepth = 0.75 * platenDepth

# Dynamic "Striker" loading
maxVelocity = 1.44/1000  # m/s converted to mm/us
targetDisplacement = 0.243 # mm
thisEndTime = 320
targetDomainStrain = targetDisplacement/maxDomainLength

strikerTime = ( targetDomainStrain * maxDomainLength * np.pi )/(2.0 * maxVelocity)

# targetDisplacement = 0.243 #0.4 # mm
targetRelaxDisp = 2.0*targetDisplacement # 0.243-0.175
thisEndStretch = 1 - targetDisplacement/maxDomainLength
thisEndRelax = thisEndStretch + targetRelaxDisp/maxDomainLength 
endExTime = 5.0 * strikerTime

# key values:
#------------------------------------------------------------------------------#
thisSolverProfiling = 0		# 0/1 no/yes dump to console

# IDOX: E=22GPa,ν=0.25,ρ=2.001mg/mm3
# Estane: E=0.002GPa,ν=0.499,ρ=1.190mg/mm3
# Estane Matrix: E=0.25GPa,ν=0.4,ρ=1.935mg/mm3

grainDiameter = 0.0225             #  what does this even do?
binderThickness = 0.02

# Idox 
# sampleDensity = 2.001 #mg/mm3
idoxYoungsModulus = 16.75 # 22.0  # GPa
idoxPoissonRatio = 0.425 #0.25

idoxDensity = 2.001				# Idox density
idoxBulkModulus = idoxYoungsModulus / (3*(1-2*idoxPoissonRatio))			# idox bulk
idoxShearModulus = idoxYoungsModulus / (2*(1+idoxPoissonRatio))		# idox shear
idoxTensileStrength = 0.113		# idox GPA
idoxCompressiveStrengthScale = 6.0  # Mike's 8ish-to-1 rule of thumb
idoxCompressiveStrength = idoxCompressiveStrengthScale*idoxTensileStrength #temp

thisWeibullSeed = 1
idoxWeibullModulus = 2.5        # __WEIBULL_MODULUS__
estaneWeibullModulus = 8.0
thisWeibullVolume = 1.0e-5      # __WEIBULL_VOLUME__ Picked to give good results

estaneMBulkModulus = estaneMYoungsModulus / (3*(1-2*estaneMPoissonRatio))			# estane bulk
estaneMShearModulus = estaneMYoungsModulus / (2*(1+estaneMPoissonRatio))		# estane shear
estaneMCompressiveStrength = estaneMCompressiveStrengthScale * estaneMTensileStrength #temporarily

#------------------------------------------------------------------------------#

# Domain -----------------------------------------------------------------------
pfw["xpar"] = 1*(partitionsPerDirection)  	# grid partitions form Cube
pfw["ypar"] = 1*(partitionsPerDirection)
pfw["zpar"] = 1*(partitionsPerDirection)

cpp=cellsPerPartition
pfw["nI"]=2+pfw["xpar"]*cpp  # grid cells in the x-direction
pfw["nJ"]=2+pfw["ypar"]*cpp  # grid cells in the y-direction
pfw["nK"]=  pfw["zpar"]*cpp  # grid cells in the z-direction
pfw["ppc"]=2   				 # particles per cell in each direction

pfw["xmin"] = -maxDomainLength/2 		# mm
pfw["xmax"] =  maxDomainLength/2 		# mm
pfw["ymin"] = -maxDomainLength/2 		# mm
pfw["ymax"] =  maxDomainLength/2 		# mm
pfw["zmin"] = 0.0 									# mm
pfw["zmax"] = maxDomainLength 			# ct_maxZ # mm

# specify an array with all objects to be included, order matters. 
# for overlapping objects, the first one listed will be assigned at each point.
# "fill" must be last on the list.
dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

basePlaten = geom.box('base', [-100, -100, -100], [100, 100, basePlatenDepth], 
	v=[0,0,0], mat=0, group=0)

topPlaten = geom.box('top', [-100, -100, pfw["zmax"]-topPlatenDepth], [100, 100, 100], 
	v=[0,0,0], mat=0, group=0)

ppvc = 10  
thisFlawSize = np.min([dx,dy,dz])*ppvc     
thisVMin=dx*dy*dz

# ctScene has binder flagged as 1 and grains numbered, but they are all disjoint
ctBinder = np.array(binderScene)
ctBinder[ ctBinder != 0 ] = 1

singleCtStackBinder = geom.ctScene('binder', ctBinder,  
	[ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
	[ct_maxX , ct_maxY, (ct_nk * ct_voxelSize)+basePlatenDepth-ct_minZ], 
	mpm_delta=[dx,dy,dz], v=[0.0, 0.0, 0.0], mat=2, group=0 )

vwBinder = geom.voronoiWeibullBoxWrapper('vw',singleCtStackBinder,
	[ct_minX - thisFlawSize, ct_minY - thisFlawSize, basePlatenDepth - thisFlawSize],
	[ct_maxX + thisFlawSize, ct_maxY + thisFlawSize, pfw["zmax"] - topPlatenDepth + thisFlawSize],
	thisFlawSize, thisWeibullVolume, estaneMWeibullModulus, thisWeibullSeed, thisVMin)

objects = []

processCTGrainAsSingleCrystal=False
# Single Idox Crystal Stack
if processCTGrainAsSingleCrystal:

	# ctScene has binder flagged as 1 and grains numbered
	ctGrains = np.array(ctScene)
	ctGrains[ctGrains == 1] = 0
	ctGrains[ ctGrains != 0 ] = 1

	singleCtStackGrains = geom.ctScene('grains', ctGrains,  
		[ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
		[ct_maxX , ct_maxY, ct_maxZ+basePlatenDepth-ct_minZ], 
		mpm_delta=[dx,dy,dz], v=[0.0, 0.0, 0.0], mat=1, group=0 )

	vwGrains = geom.voronoiWeibullBoxWrapper('vw',singleCtStackGrains,
		[ct_minX - thisFlawSize, ct_minY - thisFlawSize, basePlatenDepth - thisFlawSize],
		[ct_maxX + thisFlawSize, ct_maxY + thisFlawSize, pfw["zmax"] - topPlatenDepth + thisFlawSize],
		thisFlawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, thisVMin)

else:

	# ctScene has binder flagged as 1 and grains numbered
	ctGrains = np.array(ctScene)
	ctGrains[ctGrains == 1] = 0

	# ctScene has binder flagged as 1 and grains numbered, but they are all disjoint
	ctBinder = np.array(binderScene)
	ctBinder[ ctBinder != 0 ] = 1

	print()
	print("------------------------------------------------------------------------------")
	print("Processing grains:")

	numGrains = np.max(ctGrains)
	unique_values = np.unique(ctGrains)[1:]

	print( f"numGrains: {numGrains} ")
	print( f"unique_values: {unique_values}" )
	print( f"length of unique values: {len(unique_values)}")
	debugGrainCount=25 # len(unique_values)
	runningGrainCount=0

	for grainID in unique_values[:debugGrainCount]: 
		runningGrainCount = runningGrainCount + 1

		# # helper script to process idox crystals:
		# def process_grain(grainID, ctGrains, ct_minX, ct_minY, basePlatenDepth, ct_maxX,
		#                   ct_maxY, ct_maxZ, dx, dy, dz, idoxTensileStrength, thisWeibullSeed, 
		#                   thisWeibullVolume, idoxWeibullModulus, thisFlawSize, thisVMin):

		objects.append( process_grain(grainID, ctGrains, ct_minX, ct_minY, basePlatenDepth, ct_maxX,
	                  ct_maxY, ct_maxZ, dx, dy, dz, idoxTensileStrength, thisWeibullSeed, 
	                  thisWeibullVolume, idoxWeibullModulus, thisFlawSize, thisVMin) )

		tempGrain = 0.0*ctGrains
		tempGrain[ctGrains == grainID] = 1.0

		globals()[f"grain_{grainID}"] = geom.ctScene(f"grain_{grainID}", tempGrain,  
			[ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
			[ct_maxX , ct_maxY, ct_maxZ+basePlatenDepth-ct_minZ], 
			mpm_delta=[dx,dy,dz], v=[0.0, 0.0, 0.0], mat=0, group=0 )

		tensileStrength = idoxTensileStrength
		if thisWeibullSeed == 0:
		      # approxVol = np.count_nonzero(tempGrain) * (ct_voxelSize**3.0)
		      idoxTensileStrength = idoxTensileStrength #* (idoxWeibullModulus / approxVol)**(1/idoxWeibullModulus)  # GPa
		      idoxCompressiveStrength = idoxCompressiveStrengthScale * idoxTensileStrength 

		      objects = objects + [globals()[f"grain_{grainID}"]]
		else:    
			idoxCompressiveStrength = idoxCompressiveStrengthScale * idoxTensileStrength 

			globals()[f"vwGrain_{grainID}"] = geom.voronoiWeibullBoxWrapper(name='vw',subObject=globals()[f"grain_{grainID}"],
			  x0=[ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
			  x1=[ct_maxX, ct_maxY, (ct_nk * ct_voxelSize)+basePlatenDepth-ct_minZ], 
			  flawSize=thisFlawSize, weibullVolume=thisWeibullVolume, weibullModulus=idoxWeibullModulus, 
			  weibullSeed=thisWeibullSeed, vMin=thisVMin)

			objects = objects + [globals()[f"vwGrain_{grainID}"]]

		progress = f"\rProcessing: {runningGrainCount}/{debugGrainCount} [{int((runningGrainCount/debugGrainCount)*100)}%] {'#' * int((runningGrainCount/debugGrainCount)*20)}{' ' * (20 - int((runningGrainCount/debugGrainCount)*20))}"
		print(progress, end='', flush=True)

pfw["objects"] = objects + [topPlaten, basePlaten, vwBinder] 
numObjects = len(pfw["objects"])
print( f"number of objects in pfw object list: {numObjects} ")
# print( pfw["objects"])
# print( )
# print( objects )

# pfw["objects"] = [topPlaten, vwGrains, vwBinder, basePlaten] 

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = thisEndTime
pfw["plotInterval"] = thisEndTime / 180.0
pfw["restartInterval"] = thisEndTime / 60
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = thisEndTime / 7500.0
pfw["boxAverageHistory"] = 0

pfw["updateMethod"]="XPIC"
pfw["updateOrder"]=2
pfw["useAPIC"]=0
pfw["useInteralForceAsFaceReaction"]=1

pfw["timeIntegrationOption"]="ExplicitDynamic"
pfw["cflFactor"]=0.4   
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 0, 0, 0, 0, 2, 2 ]
pfw["prescribedBcTable"] = 0
pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'

# FTable:
# Dynamice "Striker" loading, see Idox-estane 1a
if loadingOption == 1:
	pfw["fTable"]=[[-strikerTime, 1, 1, 2 - thisEndStretch],
	[strikerTime, 1, 1, thisEndStretch],
	[endExTime, 1, 1, thisEndRelax]]
elif loadingOption == 2:
	pfw["fTable"]=[[0, 1, 1, 1],
	[strikerTime, 1, 1, thisEndStretch],
	[endExTime, 1, 1, thisEndRelax]]
else:
	raise SystemExit("Invalid loadingOption given.")

pfw["damageFieldPartitioning"] = 1
pfw["needsNeighborList"] = 1
pfw["neighborRadius"] = -1.01
pfw["useDamageAsSurfaceFlag"] = 1
pfw["frictionCoefficient"] = 0.15

# pfw["materials"] = [ "Sapphire", "Idox"] #, "EstaneMatrix" ]
pfw["materials"] = [ "Idox" ]
# <ElasticIsotropic
# 	name="Sapphire"
# 	defaultDensity="3.5"
# 	defaultBulkModulus="315"
# 	defaultShearModulus="172"/>
pfw["materialPropertyString"]="""
<CeramicDamage
	name="Idox"
	defaultDensity="""+"\""+str(idoxDensity)+"\""+"""
	defaultBulkModulus="""+"\""+str(idoxBulkModulus)+"\""+"""
	defaultShearModulus="""+"\""+str(idoxShearModulus)+"\""+"""
	tensileStrength="""+'"'+str(idoxTensileStrength)+'"'+"""
	compressiveStrength="""+'"'+str(idoxCompressiveStrength)+'"'+"""
	maximumStrength="""+'"'+str(1.25* idoxCompressiveStrength)+'"'+"""
	crackSpeed="1.8"
	/>
"""
# <CeramicDamage
# 	name="EstaneMatrix"
# 	defaultDensity="""+"\""+str(estaneMDensity)+"\""+"""
# 	defaultBulkModulus="""+"\""+str(estaneMBulkModulus)+"\""+"""
# 	defaultShearModulus="""+"\""+str(estaneMShearModulus)+"\""+"""
# 	tensileStrength="""+'"'+str(estaneMTensileStrength)+'"'+"""
# 	compressiveStrength="""+'"'+str(estaneMCompressiveStrength)+'"'+"""
# 	maximumStrength="""+'"'+str(1.25* estaneMCompressiveStrength)+'"'+"""
# 	crackSpeed="2.5"
# 	/>
# """

# Process values:
# #------------------------------------------------------------------------------#
# pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
# pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
# #------------------------------------------------------------------------------#

# pfw["plottableFields"]=["particleID",
# 						"particleMass",
# 						"particleDensity",
# 						"particleCenter",
# 						"particleReferencePosition",
# 						"particleBodyForce", 
# 						"particleStress", 
# 						"particleDamage"	]

# Define pfw["particleFileFields"]
#------------------------------------------------------------------------------#
pfw["particleFileFields"] = ["Velocity", "MaterialType", "ContactGroup", 
							 "SurfaceFlag", "StrengthScale", "RVector"]

#------------------------------------------------------------------------------#
pfw["plottableFields"] =["particleReferencePosition", "particleDensity",
 "particleMaterialType", "particleCenter", "particleVelocity", "particleVolume",
 "particleID", "particleAcceleration", "particleStress", "particleStrengthScale", 
 "particleDamage"]

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
#------------------------------------------------------------------------------#

# bgBinder = geom.CylinderPrill(
# 	'prill',[0.0, 0.0, platenDepth], [0.0, 0.0, platenDepth + (ct_nk * ct_voxelSize)+platenDepth+ct_minZ ], 
# 	1.0*(np.max( [ ct_maxX - ct_xCenter, ct_maxY - ct_yCenter ] )) , 
# 	[0.0,0.0,0.0], grainDiameter, binderThickness, 1,2, 0,0)

# # class voronoiWeibullBox:
# dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
# dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
# dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# vwBgBinder = geom.voronoiWeibullBox('vw',bgBinder,
#   [pfw["xmin"] - flawSize, pfw["ymin"] - flawSize, platenDepth - flawSize],
#   [pfw["xmax"] + flawSize, pfw["xmax"] + flawSize, pfw["zmax"] - platenDepth + flawSize],
#   flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)

# ctStackBinder = geom.ctScene('Binder', ctBinder,  
# 		[ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
# 		[ct_maxX , ct_maxY, (ct_nk * ct_voxelSize)+basePlatenDepth-ct_minZ], 
# 		[0.0, 0.0, 0.0], 2, 0, 0 )

# vwBinder = geom.voronoiWeibullBox('vwBinder',ctStackBinder,
# 		  [ct_minX, ct_minY, basePlatenDepth-ct_minZ], 
# 		  [ct_maxX, ct_maxY, (ct_nk * ct_voxelSize)+basePlatenDepth-ct_minZ], 
# 		  flawSize, thisWeibullVolume,
# 		  estaneMWeibullModulus, thisWeibullSeed, vMin)

# # # Estane
# # sampleDensity = 1.190 #mg/mm3
# estaneYoungsModulus = 0.002 # GPa
# estanePoissonRatio = 0.475