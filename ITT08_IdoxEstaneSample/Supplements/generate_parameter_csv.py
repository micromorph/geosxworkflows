import csv
import random
import argparse

def generate_random_data(num_rows, init_entry):
    data = []
    for sim_id in range(init_entry, num_rows + init_entry):
        density = 1.935
        
        # youngs_modulus = round(random.uniform(1.5, 9.0), 4)      # test batch 1 (E)
        # youngs_modulus = round(random.uniform(1.5, 12.5), 4)       # test batch 2 (E)
        youngs_modulus = round(random.uniform(2.5, 15.0), 4)       # test batch 3 (E)
        # tensile_strength = round(random.uniform(0.01, 0.03), 4)  # test batch 1 (sT)
        # tensile_strength = round(random.uniform(0.04, 0.15), 4)    # test batch 2 (sT)
        tensile_strength = round(random.uniform(0.125, 0.25), 4)    # test batch 3 (sT)
        

        poissons_ratio = round(random.uniform(0.2, 0.35), 4)  # Uniform( [0.1, 0.45] ) # pt2 crops at 0.35 to test of instability
        weibull_modulus = round(random.uniform(3.0, 6.0), 4)  
        compressive_multiple = round(random.uniform(3.5, 8.0), 4)
        stack_id = random.randint(1, 1)  # Uniform( Int[1,1] ) is an id flag for tif stacks
        
        data.append([sim_id, stack_id, density, youngs_modulus, poissons_ratio, weibull_modulus, tensile_strength, compressive_multiple])
    
    return data

def write_to_csv(filename, num_rows, init_entry):
    headers = [
        "Sim ID",
        "Stack ID", 
        "Density", 
        "Youngs Modulus (GPa)", 
        "Poisson's Ratio", 
        "Weibull modulus",
        "Tensile Strength (GPa)", 
        "Compressive Multiple"
    ]
    
    data = generate_random_data(num_rows, init_entry)
    
    with open(filename, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(headers)
        writer.writerows(data)
    
    print(f"CSV file '{filename}' created with {num_rows} rows.")
    
    # Print column headers
    headers = ["#", "Stack ID", "rho", "   E (GPa)", " nu", "   m", "   s (GPa)", "alpha_C"]
    print(", ".join(headers))

    # Print each entry as a row with formatting
    for entry in data:
        print(", ".join(f"{x:.4f}" if isinstance(x, float) else str(x) for x in entry))

def main():
    parser = argparse.ArgumentParser(description='Generate a CSV file with random data.')
    parser.add_argument('-o', '--output-file', type=str, required=False, default="output.csv", help='Name of the output CSV file')
    parser.add_argument('-n', '--num-entries', type=int, required=True, help='Number of rows to generate')
    parser.add_argument('-i', '--init-entry', type=int, required=False, default=1, help='Initial simulation id to easily be appended to another setup csv')

    args = parser.parse_args()
    
    write_to_csv(args.output_file, args.num_entries, args.init_entry)

if __name__ == "__main__":
    main()
