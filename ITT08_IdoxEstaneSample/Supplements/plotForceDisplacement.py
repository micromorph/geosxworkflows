# -*- coding: utf-8 -*-
"""
Created on Mon March 11th 03/11/2024
@author: appleton
Supplemental plot nominal force displacement 
"""
import numpy as np                   # math stuff
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import pandas as pd
import os
import re
from scipy.signal import medfilt
from pathlib import Path

def lighten_color(color, amount=0.5):
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])

maxDisp = 0.243
maxForce = 600.0
measure_displacement = 0.01
numSimsUnderMaxForce = 0
limitByMaxForce = True

simEndTime=320
simEndDisplacement=0.243

# Dynamic "Striker" loading
maxVelocity = 1.44/1000  # m/s converted to mm/us
maxDomainLength = 6.32284767875

targetDomainStrain = simEndDisplacement/maxDomainLength

strikerTime = ( targetDomainStrain * maxDomainLength * np.pi )/(2.0 * maxVelocity)

# Define parent directories
runLocation ='/p/lustre1/appleton/idoxEstaneStudies/globalSensitivityStudy/'  # parent directory of simulations
# parentDirectory/    ( i.e. runLocation )
# |
# | -- simulationDirectory1/
#       |
#       | -- reactionHistory.csv
#       | -- simulationInfo.data
# | -- simulationDirectory2/
#       |
#       | -- reactionHistory.csv
#       | -- simulationInfo.data
# | -- simulationDirectory3/
#       |
#       | -- reactionHistory.csv
#       | -- simulationInfo.data
# | -- etc ... 


# List directories and create full paths
simFiles = [os.path.join(runLocation, d) for d in os.listdir(runLocation) if os.path.isdir(os.path.join(runLocation, d))]

# If you would like to filter by simIDs:
filterSimIDs = ['sim999', 'sim997', 'sim998', 'sim996']
# filterSimIDs = ['sim999', 'sim997', 'sim998', 'sim38','sim8', 'sim43', 'sim39', 'sim33', 'sim65', 'sim57', 'sim24', 'sim72', 'sim78', 'sim47', 'sim66', 'sim88', 'sim999', 'sim6', 'sim54', 'sim56', 'sim99', 'sim35', 'sim63', 'sim83', 'sim22', 'sim7', 'sim31', 'sim46', 'sim41', 'sim36', 'sim97']
badSims = []
goodSims = []
for file in simFiles:
	sim_id_match = re.search(r'sim(\d+)', file)

	if sim_id_match:  # Check if a sim ID is found
		sim_id = sim_id_match.group(0)  # Get the full match (e.g., 'sim8', 'sim43', etc.)

		if sim_id in filterSimIDs:  # Check if the extracted sim ID is in the list of goodSimIDs
		    goodSims.append(file)  # Add file to goodSims
		else:
		    badSims.append(file)  # Add file to badSims
	else:
		badSims.append(file)  # If no match found, treat as a bad sim

simFiles = goodSims        # uncomment if you would like to only consider good sims
# simFile = ['/p/lustre1/appleton/idoxEstaneStudies/globalSensitivityStudy/IdoxEstaneGSensitivityStudy_sim999']

#initialize plots
evenly_spaced_interval = np.linspace(0, 1, len(simFiles)+2)
colors = [cm.rainbow(x) for x in evenly_spaced_interval]
fig, (ax1, ax2, ax3) = plt.subplots(3, 1,figsize=(10.5,9), gridspec_kw={'height_ratios': [1.75, 2.75, 2.75]})
# fig, (ax2, ax3) = plt.subplots(2, 1,figsize=(12,7.5), gridspec_kw={'height_ratios': [1.5, 2.75]})

# approximate load profile for reference
times = np.linspace(0, simEndTime, int(strikerTime))
displacements = 1.25 * ( ( 1 + np.cos( np.linspace(-np.pi , 0.0, int(strikerTime)) ) )/ 2.0 ) * simEndDisplacement
ax1.plot(times, displacements, linewidth=3.5, alpha=0.35, color='black', linestyle='--')  # Dashed line)
# 0
# 1.7777777777777777
# 3.5555555555555554
# 5.333333333333333
# 7.1111111111111107
# 8.8888888888888893
# 10.666666666666666
# 12.444444444444443
# 14.222222222222221
# 16
# 17.777777777777779
# 19.555555555555557
# 21.333333333333332
# 23.111111111111111
# 24.888888888888889
# 26.666666666666664
# 28.444444444444443
# 30.222222222222221
# 31.999999999999996
# 33.777777777777771
# 35.55555555555555
# 37.333333333333329
# 39.11110440644574
# 39.594420239676374
# 40.888875062969561
# 42.666652840747339
# 44.444430618525118
# 46.222208396302896
# 47.999986174080675
# 49.777763951858454
# 51.555541729636232
# 53.333319507414011
# 55.111097285191789
# 56.888875062969568
# 58.666652840747346
# 60.444430618525125
# 62.222208396302904
# 63.999986174080682
# 65.777763951858461
# 67.555541729636232
# 69.333319507414004
# 71.111097285191775
# 72.888875062969547
# 74.666652840747318
# 76.444430618525089
# 78.222208396302861
# 78.779837844628616





vtk_timesteps = [
    0, 1.7777777777777777, 3.5555555555555554, 5.333333333333333,
    7.1111111111111107, 8.8888888888888893, 10.666666666666666,
    12.444444444444443, 14.222222222222221, 16, 17.777777777777779,
    19.555555555555557, 21.333333333333332, 23.111111111111111,
    24.888888888888889, 26.666666666666664, 28.444444444444443,
    30.222222222222221, 31.999999999999996, 33.777777777777771,
    35.55555555555555, 37.333333333333329, 39.11110440644574,
    40.888875062969561, 42.666652840747339, 44.444430618525118, 
    46.222208396302896, 47.999986174080675, 49.777763951858454, 
    51.555541729636232, 53.333319507414011, 55.111097285191789, 
    56.888875062969568, 58.666652840747346, 60.444430618525125,
    62.222208396302904, 63.999986174080682, 65.777763951858461, 
    67.555541729636232, 69.333319507414004, 71.111097285191775,
    72.888875062969547, 74.666652840747318, 76.444430618525089,
    78.222208396302861, 
]

restart_timesteps = [
39.594420239676374, 78.779837844628616
]

ax1.axvline(x=vtk_timesteps[0], color='black', linestyle='--', linewidth=1, label='vtkWrite')  # Dashed line
ax1.axvline(x=restart_timesteps[0], color='orange', linestyle='-', linewidth=10, alpha=0.2, label='restart')  # Dashed line
ax2.axvline(x=vtk_timesteps[0], color='black', linestyle='--', linewidth=1, label='vtkWrite')  # Dashed line
ax2.axvline(x=restart_timesteps[0], color='orange', linestyle='-', linewidth=10, alpha=0.2, label='restart')  # Dashed line
for i, xPoint in enumerate(vtk_timesteps[1:], start=1):
	if i%10 == 0:
		ax1.axvline(x=xPoint, color='black', linestyle='--', linewidth=1.0)  # Dashed line
		ax2.axvline(x=xPoint, color='black', linestyle='--', linewidth=1.0)  # Dashed line
	else:
		ax1.axvline(x=xPoint, color='black', linestyle='--', linewidth=1.0, alpha=0.2)  # Dashed line
		ax2.axvline(x=xPoint, color='black', linestyle='--', linewidth=1.0, alpha=0.2)  # Dashed line

for timeTarget in restart_timesteps[1:]:
	if timeTarget < 200:
		ax1.axvline(x=timeTarget, color='orange', linestyle='-', linewidth=10, alpha=0.2)
		ax2.axvline(x=timeTarget, color='orange', linestyle='-', linewidth=10, alpha=0.2)
	else:
		ax1.axvline(x=timeTarget, color='purple', linestyle='-', linewidth=8, alpha=0.2)
		ax2.axvline(x=timeTarget, color='purple', linestyle='-', linewidth=8, alpha=0.2)



plotted_fields = pd.DataFrame()
file_list = []     # initialize list of files plotted

for i,file in enumerate(simFiles):

	reactionFile=file+'/reactionHistory.csv'

	print(reactionFile)

	if os.path.exists(reactionFile):
		# area (mm^2)
		# time, F00, F11, F22, length_x, length_y, length_z, Rx-, Rx+, Ry-, Ry+, Rz-, Rz+, L00, L11, L22
		data = np.genfromtxt(reactionFile, delimiter=',')
		time = data[:,0]
		F00 = data[:,1]
		F11 = data[:,2]
		F22 = data[:,3]
		lenx = data[:,4]
		leny = data[:,5]
		lenz = data[:,6]
		Rxm = data[:,7]
		Rxp = data[:,8]
		Rym = data[:,9]
		Ryp = data[:,10]
		Rzm = data[:,11]
		Rzp = data[:,12]
		Lxx = data[:,13]
		Lyy = data[:,14]
		Lzz = data[:,15]

		# this hides all non-monotic time entries, so restart data files are cleaned:
		maxt = 0.0
		mask = np.ones(len(time), dtype=bool)
		for ii,t in enumerate(time):
			if (t<=maxt):
				mask[ii] = False
			else:
				maxt = t
		time = time[mask,...]
		F00 = F00[mask,...]
		F11 = F11[mask,...]
		F22 = F22[mask,...]
		lenx = lenx[mask, ...]
		leny = leny[mask, ...]
		lenz = lenz[mask, ...]
		Rxm = Rxm[mask,...]
		Rxp = Rxp[mask,...]
		Rym = Rym[mask,...]
		Ryp = Ryp[mask,...]
		Rzm = Rzm[mask,...]
		Rzp = Rzp[mask,...]
		Lzz = Lzz[mask,...]

		# strain
		exx=np.log(F00)
		eyy=np.log(F11)
		ezz=np.log(F22)

		print(lenz[1])

		# this is domain height instead of sample height, 
		#    but sample height can be pulled from data file if necessary
		height = lenz[1]  

		fzz = 1000 * 0.5*(-Rzp+Rzm)
		fyy = 1000 * 0.5 * ( -Ryp + Rym )
		fzzp = -1000.0*Rzp
		fzzm = 1000 * Rzm
		disp = (1-F22)*lenz[1] 
		# # may have strange writes that need to be filtered 
		# fzzp = medfilt(fzzp, kernel_size=5)
		# fzzm = medfilt(fzzm, kernel_size=5)

		# Average force
		avg_force = 0.5 * (fzzp + fzzm)

		sim_data = pd.DataFrame({
		    'Disp(um)': 1000.0 * disp[2:],
		    'Force(N)': avg_force[2:] 
		})

		max_force_index = sim_data['Force(N)'].idxmax()
		pd_disp = pd.Series(disp)
		filter_disp_index = np.abs(pd_disp - measure_displacement).idxmin()
		max_force = sim_data['Force(N)'].max()
		displacement_at_max_force = sim_data.loc[max_force_index, 'Disp(um)']

		if limitByMaxForce and np.nanmax(fzzp[:filter_disp_index]) <= maxForce and np.nanmax(fzzp) <= 3.0*maxForce:# and displacement_at_max_force < 1000.0 * np.nanmax(disp):
			
			match = re.search(r'sim(\d+)', file)

			print( "   File: ", file )
			print( "   Match: ", match)

			if match:
			    sim_id = match.group(0)  # "sim" followed by the digits
			    file_list = file_list + [sim_id]

			print("Simulation:", file, "Progress:", round( np.nanmax(time) / simEndTime * 100, 1 ))
			ax1.plot(time,disp,linestyle='-',color=colors[i],linewidth=5, label=f"{file}")
			ax1.plot(time, i * (1.25 * simEndDisplacement / (len(simFiles))) * np.ones(len(time)), linestyle='-', color=colors[i], linewidth=2, alpha=0.25 ) 
			ax2.plot(time, fzzm ,linestyle='-',color=colors[i],linewidth=1.5, label=f"Sim:{file}")
			ax2.plot(time, fzzp ,linestyle='-',color='black',linewidth=1)
			ax3.plot(disp, fzzm ,linestyle='-',color=colors[i],linewidth=1.5, label=f"Sim:{file}")
			ax3.plot(disp, fzzp ,linestyle='-',color='black',linewidth=1)

			numSimsUnderMaxForce = numSimsUnderMaxForce + 1
		elif not limitByMaxForce:
			print("Simulation:", file, "Progress:", round( np.nanmax(time) / simEndTime * 100, 1 ))
			print("Simulation:", file, "Progress:", round( np.nanmax(time) / simEndTime * 100, 1 ))
			ax1.plot(time,disp,linestyle='-',color=colors[i],linewidth=5, label=f"{file}")
			ax1.plot(time, i * (1.25 * simEndDisplacement / (len(simFiles))) * np.ones(len(time)), linestyle='-', color=colors[i], linewidth=2, alpha=0.25 ) 
			ax2.plot(time, fzzm ,linestyle='-',color=colors[i],linewidth=1.5, label=f"Sim:{file}")
			ax2.plot(time, fzzp ,linestyle='-',color='black',linewidth=1)
			ax3.plot(disp, fzzm ,linestyle='-',color=colors[i],linewidth=1.5, label=f"Sim:{file}")
			ax3.plot(disp, fzzp ,linestyle='-',color='black',linewidth=1)			

			numSimsUnderMaxForce = numSimsUnderMaxForce + 1

print("numSimsUnderMaxForce: ", numSimsUnderMaxForce)
# displacement vs time
ax1.set_xlabel(r'Time ($\mu$s)', fontsize=16)
ax1.set_ylabel('  ', fontsize=16, labelpad=10)
ax1.set_xlim(0.0,100)#simEndTime)
# ax2.legend(bbox_to_anchor=(1.04,1), loc="upper left",fontsize='medium')
ax1.grid()

# Force vs time
ax2.set_xlabel(r'Time ($\mu s$)', fontsize=16)
ax2.set_ylabel('  ', fontsize=16, labelpad=10)
# ax3.legend(bbox_to_anchor=(1.04,1), loc="upper left",fontsize='small')
ax2.grid()
# ax3.set_ylim(0,15)
ax2.set_xlim(0.0,100)#simEndTime)
# ax2.set_ylim(0,600)
# if limitByMaxForce:
# 	ax3.set_ylim(0,maxForce)

# Force vs Compressive Displacement
ax3.set_xlabel(r'Compressive Disp. ($mm$)', fontsize=16)
# ax3.set_xlabel(r'Adjusted Strain', fontsize=16)
ax3.set_ylabel('  ', fontsize=16, labelpad=10)
# ax3.legend(bbox_to_anchor=(1.04,1), loc="upper left",fontsize='small')
ax3.grid()
# ax3.set_ylim(0,15)
ax3.set_xlim(0.0,simEndDisplacement)
# ax3.set_ylim(0,600)
# if limitByMaxForce:
# 	ax3.set_ylim(0,maxForce)

# Write aligned Y axis labels
fig.text(0.01, 0.0, r'                   Comp. Force (N)                Comp. Force (N)                   Disp. ($\vert \, u(t) \, \vert$,  $mm$) [Sim]', va='bottom', rotation='vertical', fontsize=15)
# fig.text(0.01, 0.0, r'     Force per Contact Area (N$ / mm^2$)                   Disp. ($\vert \, u(t) \, \vert$,  $mm$)', va='bottom', rotation='vertical', fontsize=15)
# fig.text(0.01, 0.0, r'               $fzz^+-fzz^-$          Eng. Stress ($\sigma_{zz}$, $GPa$)             Disp. ($\vert \, u(t) \, \vert$,  $mm$)', va='bottom', rotation='vertical', fontsize=15)
fig.tight_layout()

# print(file_list)
plt.show()
