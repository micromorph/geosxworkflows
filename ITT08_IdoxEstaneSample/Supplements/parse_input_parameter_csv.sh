#!/bin/bash
#SBATCH -t 3:00:00
#SBATCH -N 1
#SBATCH -A uco


# Check if the correct number of arguments are provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <input_csv_file>"
    exit 1
fi

input_file=$1

# Check if the file exists
if [ ! -f "$input_file" ]; then
    echo "File not found: $input_file"
    exit 1
fi

# 10 ppd -> 9 Nodes  with 92.86 % usage   ( HERE Res2 )
# np.ceil( ppd**3 / 112 )
partitionsPerDirection=8 # 4-8 for testing 10 for sims
cellsPerPartition=10 #12
weibullSeed=1
# maxDomainLength=5.5
# domainScale='1.25'

tifStackParentDirectory="/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/newSegmentations"

runLocation="/p/lustre1/appleton/idoxEstaneManualCalibration"
userBank="uco"
userName="$(whoami)"
fileName="updateIdoxEstaneStack"

fileLocation='/g/g15/appleton/psaapSims/geosxworkflows/ITT08_IdoxEstaneSample'
runningFileNamePrefix="IdoxEstaneManualCalibration"
# ==========================================================================================================================================
# This should be the location of the input file and anything else you need to copy over:
pfwFileLocation='/usr/workspace/appleton/PSAAPSpace/particleFileWriter'
geosPath='/usr/workspace/appleton/PSAAPSpace/GEOS/build-dane-gcc-12-release/bin/geosx'

# def write_to_csv(filename, num_rows, init_entry):
#     headers = [
#         "Sim ID",
#         "Stack ID",
#         "Density", 
#         "Youngs Modulus (GPa)", 
#         "Poisson's Ratio", 
#         "Weibull modulus",
#         "Tensile Strength (GPa)", 
#         "Compressive Multiple"
#     ]

# these values are constant for this study
idoxDensity="2.001"                          # Idox density
idoxYoungsModulus="16.75"                   # GPa
idoxPoissonRatio="0.425"                    #
idoxWeibullModulus="4.5"                    # __WEIBULL_MODULUS__
idoxTensileStrength="0.113"                 # idox GPA
idoxCompressiveStrengthScale="8.0"         # 8ish-to-1 for pure brittle failure

maxDomainLength="6.25"
edgeLength=$(echo "scale=10; $maxDomainLength / ($partitionsPerDirection * $cellsPerPartition * 2)" | bc)
rounded_value=$(printf "%.3f" "$edgeLength")
formatted_value=$(echo "$rounded_value" | sed 's/\./p/')

# Print the header
echo "Parsing CSV file: $input_file"
echo "----------------------------------"
echo "Sim ID, Stack ID, Density, Youngs Modulus (GPa), Poisson's Ratio, Weibull modulus, Tensile Strength (GPa), Compressive Strength Multiple"
echo ""

# Process the CSV file to extract and print each row
{
    # Skip the header line and process the rest
    grep -v '^Sim ID' "$input_file" | while IFS=',' read -r sim_id stack_id density youngs_modulus poissons_ratio weibull_modulus tensile_strength compressive_multiple
    do

        # Print each row with proper formatting
        echo "Sim ID: $sim_id, Stack ID: $stack_id, Density: $density, Youngs Modulus (GPa): $youngs_modulus, Poisson's Ratio: $poissons_ratio, Weibull Modulus: $weibull_modulus, Tensile Strength (GPa): $tensile_strength, Compressive Strength Multiple: $compressive_multiple"

        # Check the value of stack_id
        if [ "$stack_id" -eq 1 ]; then
            # Action for stack_id == 1
            echo "    Processing stack_id 1"
            tifDirLocation="${tifStackParentDirectory}/I43.01_bin4_levled_radial_4msd_1ero/I43.01_bin4_levled_radial_4msd_1ero_labeled_voxels/"
            sampleName="I43.01"
            pixelSize='6.028937'
            # echo ""
        elif [ "$stack_id" -eq 2 ]; then
            # Action for stack_id == 2
            echo "    Processing stack_id 2"
            # echo ""
        else
            # Action for bad stack_id
            echo "    Error -- Bad stack_id: $stack_id. Skipping this entry."
            echo ""
            continue  # Skip to the next iteration of the loop
        fi

        # Set fileName=xxx (no spaces), where the input file is pfw_input_xxx.py 
        runningFileName=${runningFileNamePrefix}_sim${sim_id}

        #Write and possibly run files
        if [ -n "$fileName" ]
        then
            echo "    Running job: "$runningFileName
            echo "    Running job ${fileName} with name ${runningFileName}"
            echo "    Deleting ${runLocation}/${runningFileName} directory"
            rm -rf $runLocation/$runningFileName/                       

            echo "    Creating new ${runLocation}/${runningFileName} directory"                                                            # delete old results for the same fileName!!!
            mkdir -p $runLocation/$runningFileName/   
            mkdir -p $runLocation/$runningFileName/Sample_TifStack      

            echo "    Copying ct data"
            cp $tifDirLocation/* $runLocation/$runningFileName/Sample_TifStack/   # copy the ct Data

            echo "    Copying input file, pfw preprocessor, autorestart script, geometry objects, and the pfw tpls"
            cp $fileLocation/pfw_input_$fileName.py $runLocation/$runningFileName/pfw_input_$runningFileName.py          # copy the input file
            cp $pfwFileLocation/updateParticleFileWriter.py $runLocation/$runningFileName/particleFileWriter.py         # copy the preprocessor
            # cp $pfwFileLocation/particleFileWriter.py $runLocation/$runningFileName         # copy the preprocessor
            cp $pfwFileLocation/pfw_updateCheck.py $runLocation/$runningFileName/pfw_check.py  # copy the autoRestart script
            cp $pfwFileLocation/pfw_updateGeometryObjects.py $runLocation/$runningFileName/pfw_geometryObjects.py       # copy the geometry object functions
            cp -r $pfwFileLocation/thirdPartyLibraries/ $runLocation/$runningFileName       # move tpls to run location

            echo "    Moving to ${runLocation}/${runningFileName} directory"
            cd $runLocation/$runningFileName   

            # swap out parameters
            echo "    Swapping run script parameters into input file"
            sed -i "s#__THIS_PPD__#${partitionsPerDirection}#g" pfw_input_$runningFileName.py   
            sed -i "s#__THIS_CPP__#${cellsPerPartition}#g" pfw_input_$runningFileName.py  
            sed -i "s#__DOMAIN_SCALING_FACTOR__#${domainScalingFactor}#g" pfw_input_$runningFileName.py
            sed -i "s#__PIXEL_SIZE__#${pixelSize}#g" pfw_input_$runningFileName.py

            sed -i "s#__EM_DENSITY__#${density}#g" pfw_input_$runningFileName.py
            sed -i "s#__EM_YOUNGS_MODULUS__#${youngs_modulus}#g" pfw_input_$runningFileName.py
            sed -i "s#__EM_POISSONS_RATIO__#${poissons_ratio}#g" pfw_input_$runningFileName.py
            sed -i "s#__EM_WEIBULL_MODULUS__#${weibull_modulus}#g" pfw_input_$runningFileName.py
            sed -i "s#__EM_TENSILE_STRENGTH__#${tensile_strength}#g" pfw_input_$runningFileName.py
            sed -i "s#__EM_COMPRESSIVE_STRENGTH_SCALE__#${compressive_multiple}#g" pfw_input_$runningFileName.py
            
            sed -i "s#__IDOX_DENSITY__#${idoxDensity}#g" pfw_input_$runningFileName.py
            sed -i "s#__IDOX_YOUNGS_MODULUS__#${idoxYoungsModulus}#g" pfw_input_$runningFileName.py
            sed -i "s#__IDOX_POISSONS_RATIO__#${idoxPoissonRatio}#g" pfw_input_$runningFileName.py
            sed -i "s#__IDOX_WEIBULL_MODULUS__#${idoxWeibullModulus}#g" pfw_input_$runningFileName.py
            sed -i "s#__IDOX_TENSILE_STRENGTH__#${idoxTensileStrength}#g" pfw_input_$runningFileName.py
            sed -i "s#__IDOX_COMPRESSIVE_STRENGTH_SCALE__#${idoxCompressiveStrengthScale}#g" pfw_input_$runningFileName.py

            # Create userDefs file
            echo "    Creating userDefs file"
            echo "# -*- coding: utf-8 -*-" > userDefs_$(whoami).py
            echo "geosPath=\"${geosPath}\"" >> userDefs_$(whoami).py
            echo "testRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
            echo "defaultRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
            echo "defaultBank=\"${userBank}\"" >> userDefs_$(whoami).py

            echo "    Writing simulation info file"
            bulkModulus=$(echo "scale=6; (${youngs_modulus})/(3.0*(1.0-2.0*${poissons_ratio}))" | bc)  
            shearModulus=$(echo "scale=6; (${youngs_modulus})/(2.0*(1.0+${poissons_ratio}))" | bc)  
            echo "" > simulation.info
            echo "Simulation_ID: ${sim_id}" >> simulation.info
            echo "Idox Estane Info: " >> simulation.info  
            echo "   sampleName:  ${sampleName}" >> simulation.info 
            echo "   pixelSize:  ${pixelSize}" >> simulation.info 
            echo "Estane Matrix parameters " >> simulation.info 
            echo "   estaneMDensity_mgBymm3:  ${density}" >> simulation.info 
            echo "   estaneMYoungsModulus_GPa:  ${youngs_modulus}" >> simulation.info 
            echo "   estaneMPoissonRatio:  ${poissons_ratio}" >> simulation.info 
            echo "   estaneMBulkModulus_GPa: $bulkModulus" >> simulation.info
            echo "   estaneMShearModulus_GPa: $shearModulus" >> simulation.info
            echo "   estaneMWeibullModulus:  ${weibull_modulus}" >> simulation.info 
            echo "   estaneMTensileStrength_GPa:  ${tensile_strength}" >> simulation.info 
            echo "   estaneMCompressiveStrengthScale:  ${compressive_multiple}" >> simulation.info 
            echo "Idox parameters " >> simulation.info 
            echo "   idoxDensity_mgBymm3:  ${idoxDensity}" >> simulation.info 
            echo "   idoxYoungsModulus_GPa:  ${idoxYoungsModulus}" >> simulation.info 
            echo "   idoxPoissonRatio:  ${idoxPoissonRatio}" >> simulation.info 
            echo "   idoxWeibullModulus:  ${idoxWeibullModulus}" >> simulation.info 
            echo "   idoxTensileStrength_GPa:  ${idoxTensileStrength}" >> simulation.info 
            echo "   idoxCompressiveStrengthScale:  ${idoxCompressiveStrengthScale}" >> simulation.info 
            echo "Resolution parameters " >> simulation.info 
            echo "   partitionsPerDirection:  ${partitionsPerDirection}" >> simulation.info 
            echo "   cellsPerPartition:  ${cellsPerPartition}" >> simulation.info 
            echo "   particlesPerCellPerDirection:  2 (default)" >> simulation.info 
            echo "   domainLength_mm:  ${maxDomainLength}" >> simulation.info  
            echo "   materialPointParticleEdgeLength_mm:  ${edgeLength}" >> simulation.info 
            echo "" >> simulation.info

            echo "    Information written to simulation.info"
            
            python3 particleFileWriter.py pfw_input_$runningFileName.py                        # launch the VML
        fi

    echo "Completing simulation ${sim_id} launch."
    echo ""

    done
} < "$input_file"
