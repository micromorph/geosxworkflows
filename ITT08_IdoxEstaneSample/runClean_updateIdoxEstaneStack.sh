#!/bin/bash
#SBATCH -t 3:00:00
#SBATCH -N 1
#SBATCH -A uco

# Set fileName=xxx (no spaces), where the input file is pfw_input_xxx.py 

serial_pfw=True
# n_cores still needs to be set so that there is sufficient allocation 
#     memory for the check.  there is about 2.3Gb / core
n_cores=36   
fileName="updateIdoxEstaneStack"   #idea_test"
fileLocation='/g/g15/appleton/psaapSims/geosxworkflows/ITT08_IdoxEstaneSample'

partitionsPerDirection="6"   # 4, 8, 12
cellsPerPartition="10"       # 12 is what i've been using, but i can use more on the debug to get slightly better resolution

# SPHB UQ Parameters:
# ----- Estane Matrix ----------------------------------------------------------
estaneMYoungsModulus="10.5" #"0.350"             # GPa originally 250 MPa (the MPa range is quasistatic, apparent youngs modulus from SHPB is GPa range)
estaneMPoissonRatio="0.275"
estaneMWeibullModulus="3.5"
estaneMTensileStrength="0.100"             # estane GPA original 0.2

runningFileName=test_updatePFW_idoxEstanePuck # this sets the run location directory name

# Other Parameters that are assumed to be known
# ----- Estane Matrix ----------------------------------------------------------
estaneMDensity="1.935"                   # estane matrix density
estaneMCompressiveStrengthScale="4.0"    # 8ish-to-1 for pure brittle failure
# ----- Idox -------------------------------------------------------------------
idoxDensity="2.001"                          # Idox density
idoxYoungsModulus="16.75"                   # GPa
idoxPoissonRatio="0.425"                    #
idoxWeibullModulus="4.5"                    # __WEIBULL_MODULUS__
idoxTensileStrength="0.113"                 # idox GPA
idoxCompressiveStrengthScale="8.0"         # 8ish-to-1 for pure brittle failure
# ----- Others -----------------------------------------------------------------
loadingOption="2"    # 1 is impact, 2 is ramped


# highest resolution at 25 ppd -> 121 nodes on dane
#   to double resolution of 16 ppd, 12 cpp, we'll target 25 ppd with 15 cpp at 9um edge length
# sampleName="I43.01_BH_2xDS/I43.01_BH_2xDS_labeled_voxels/"
# sampleName="/I43.01_bin4_levled_radial_4msd_1ero/I43.01_bin4_levled_radial_4msd_1ero_labeled_voxels/"
sampleName="/I43.02_bin4_leveled_radial_4msd_1ero/I43.02_bin4_leveled_radial_4msd_1ero_labeled_voxels/"
# tifDirLocation="/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/${sampleName}"
tifDifLocation="/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/newSegmentations/${sampleName}"
# A. Input:
#   01. Input dir path: C:\Users\nepeterson_a\I43_scratch\I43_01\2xDS
#   02. File suffix: .tiff
#   03. Slice crop:
#   - 110
#   - 810    # 700 slices
#   04. Row crop:
#   - 70
#   - 910    # 
#   05. Column crop:
#   - 60
#   - 900
#   06. Pixel size: 6.028937

# 215.tif TIFF 840x840 840x840+0+0 16-bit Grayscale Gray 1.34607MiB 0.000u 0:00.000

# pixelSize="6.028937"    #um
pixelSize="12.058"

# runningFileName="I43p02" # this sets the run location directory name
# tifDirLocation='/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/I43.02_BH_2xDS/I43.02_BH_2xDS_labeled_voxels/'

# runningFileName="I43p03" # this sets the run location directory name
# tifDirLocation='/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/I43.03_BH_2xDS/I43.03_BH_2xDS_labeled_voxels/'

# runningFileName="I43p04" # this sets the run location directory name
# tifDirLocation='/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/I43.04_BH_2xDS/I43.04_BH_2xDS_labeled_voxels/'

# runningFileName="I43p05" # this sets the run location directory name
# tifDirLocation='/usr/workspace/appleton/SupplementalFiles/IdoxSamples/IDOX_pucks/I43.05_BH_4xDS/I43.05_BH_4xDS_labeled_voxels/'

maxDomainLength=6.25
# domainScalingFactor=1.25
# Calculate edgeLength
edgeLength=$(echo "scale=10; $maxDomainLength / ($partitionsPerDirection * $cellsPerPartition * 2)" | bc)
rounded_value=$(printf "%.3f" "$edgeLength")
formatted_value=$(echo "$rounded_value" | sed 's/\./p/')

# if [ "$loadingOption" = 1 ]; then
#         runningFileName="${runningFileName}_h${formatted_value}_impact"
# fi
# if [ "$loadingOption" = 2 ]; then
#         runningFileName="${runningFileName}_h${formatted_value}_ramped"
# fi

runLocation='/p/lustre1/appleton/fullPuckTest'
userBank="uco"
userName="$(whoami)"

# ==========================================================================================================================================

pfwFileLocation='/usr/workspace/appleton/PSAAPSpace/particleFileWriter'
geosPath='/usr/workspace/appleton/TestSpace/GEOS/build-dane-gcc-12-release/bin/geosx'
# geosPath='/usr/workspace/appleton/PSAAPSpace/GEOS/build-dane-gcc-12-release/bin/geosx'

if [ -n "$fileName" ]
then
        echo "    Running job: "$runningFileName
        echo "    Running job ${fileName} with name ${runningFileName}"
        echo "    Deleting ${runLocation}/${runningFileName} directory"
        rm -rf $runLocation/$runningFileName/                       
        echo "    Creating new ${runLocation}/${runningFileName} directory"     # create the run/output directory                                                            # delete old results for the same fileName!!!
        mkdir -p $runLocation/$runningFileName/   
        mkdir -p $runLocation/$runningFileName/Sample_TifStack      

        echo "    Copying ct data"
        cp $tifDirLocation/* $runLocation/$runningFileName/Sample_TifStack/   # copy the ct Data

        echo "    Copying input file, pfw preprocessor, autorestart script, geometry objects, and the pfw tpls"
        cp $fileLocation/pfw_input_$fileName.py $runLocation/$runningFileName/pfw_input_$runningFileName.py          # copy the input file
        cp $pfwFileLocation/updateParticleFileWriter.py $runLocation/$runningFileName/particleFileWriter.py         # copy the preprocessor
        # cp $pfwFileLocation/particleFileWriter.py $runLocation/$runningFileName         # copy the preprocessor
        cp $pfwFileLocation/pfw_updateCheck.py $runLocation/$runningFileName/pfw_check.py  # copy the autoRestart script
        cp $pfwFileLocation/pfw_updateGeometryObjects.py $runLocation/$runningFileName/pfw_geometryObjects.py       # copy the geometry object functions
        cp -r $pfwFileLocation/thirdPartyLibraries/ $runLocation/$runningFileName       # move tpls to run location

        echo "    Moving to ${runLocation}/${runningFileName} directory"
        cd $runLocation/$runningFileName  

        # swap out parameters
        echo "    Swapping run script parameters into input file"
        sed -i "s#__LOADING_OPTION__#${loadingOption}#g" pfw_input_$runningFileName.py   
        sed -i "s#__THIS_PPD__#${partitionsPerDirection}#g" pfw_input_$runningFileName.py   
        sed -i "s#__THIS_CPP__#${cellsPerPartition}#g" pfw_input_$runningFileName.py  
        sed -i "s#__DOMAIN_SCALING_FACTOR__#${domainScalingFactor}#g" pfw_input_$runningFileName.py
        sed -i "s#__PIXEL_SIZE__#${pixelSize}#g" pfw_input_$runningFileName.py

        sed -i "s#__EM_DENSITY__#${estaneMDensity}#g" pfw_input_$runningFileName.py
        sed -i "s#__EM_YOUNGS_MODULUS__#${estaneMYoungsModulus}#g" pfw_input_$runningFileName.py
        sed -i "s#__EM_POISSONS_RATIO__#${estaneMPoissonRatio}#g" pfw_input_$runningFileName.py
        sed -i "s#__EM_WEIBULL_MODULUS__#${estaneMWeibullModulus}#g" pfw_input_$runningFileName.py
        sed -i "s#__EM_TENSILE_STRENGTH__#${estaneMTensileStrength}#g" pfw_input_$runningFileName.py
        sed -i "s#__EM_COMPRESSIVE_STRENGTH_SCALE__#${estaneMCompressiveStrengthScale}#g" pfw_input_$runningFileName.py
        
        sed -i "s#__IDOX_DENSITY__#${idoxDensity}#g" pfw_input_$runningFileName.py
        sed -i "s#__IDOX_YOUNGS_MODULUS__#${idoxYoungsModulus}#g" pfw_input_$runningFileName.py
        sed -i "s#__IDOX_POISSONS_RATIO__#${idoxPoissonRatio}#g" pfw_input_$runningFileName.py
        sed -i "s#__IDOX_WEIBULL_MODULUS__#${idoxWeibullModulus}#g" pfw_input_$runningFileName.py
        sed -i "s#__IDOX_TENSILE_STRENGTH__#${idoxTensileStrength}#g" pfw_input_$runningFileName.py
        sed -i "s#__IDOX_COMPRESSIVE_STRENGTH_SCALE__#${idoxCompressiveStrengthScale}#g" pfw_input_$runningFileName.py

        # Create userDefs file
        echo "    Creating userDefs file"
        echo "# -*- coding: utf-8 -*-" > userDefs_$(whoami).py
        echo "geosPath=\"${geosPath}\"" >> userDefs_$(whoami).py
        echo "testRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
        echo "defaultRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
        echo "defaultBank=\"${userBank}\"" >> userDefs_$(whoami).py

        # echo "  Launching particle file writer ... "
        # python3 particleFileWriter.py pfw_input_$runningFileName       # launch the preprocessor

        echo "    Writing parallel particle file writer launch script ... "
        timestamp=$(date +"%Y%m%d%H%M%S")
        pfwLaunchFileName="${timestamp}_PfwLaunch.sh"
        touch $pfwLaunchFileName
        echo "#!/bin/bash" > $pfwLaunchFileName
        echo "#SBATCH -t 02:30:00" >> $pfwLaunchFileName
        echo "#SBATCH -N 1" >> $pfwLaunchFileName
        echo "#SBATCH -n $n_cores" >> $pfwLaunchFileName
        echo "#SBATCH -J pfw_${runningFileName}_Launch" >> $pfwLaunchFileName
        echo "#SBATCH -A ${userBank}" >> $pfwLaunchFileName
        echo "#SBATCH -p pbatch" >> $pfwLaunchFileName
        echo "" >> $pfwLaunchFileName
        echo 'echo "Launching srun -N1 -n100 particleFileWriter.py pfw_input_${runningFileName} ... "' >> $pfwLaunchFileName
        echo "srun -N1 -n$n_cores python3 particleFileWriter.py pfw_input_$runningFileName" >> $pfwLaunchFileName
        echo 'echo "srun command has completed, good bye. "' >> $pfwLaunchFileName

        echo "    Writing simulation info file"
        echo "Idox Estane Info: " > simulation.info  
        echo "   sampleName:  ${sampleName}" >> simulation.info 
        echo "   pixelSize:  ${pixelSize}" >> simulation.info 
        echo "Estane Matrix parameters " >> simulation.info 
        echo "   estaneMDensity_mgBymm3:  ${estaneMTensileStrength}" >> simulation.info 
        echo "   estaneMYoungsModulus_GPa:  ${estaneMYoungsModulus}" >> simulation.info 
        echo "   estaneMPoissonRatio:  ${estaneMPoissonRatio}" >> simulation.info 
        echo "   estaneMWeibullModulus:  ${estaneMWeibullModulus}" >> simulation.info 
        echo "   estaneMTensileStrength_GPa:  ${estaneMTensileStrength}" >> simulation.info 
        echo "   estaneMCompressiveStrengthScale:  ${estaneMCompressiveStrengthScale}" >> simulation.info 
        echo "Idox parameters " >> simulation.info 
        echo "   idoxDensity_mgBymm3:  ${idoxDensity}" >> simulation.info 
        echo "   idoxYoungsModulus_GPa:  ${idoxYoungsModulus}" >> simulation.info 
        echo "   idoxPoissonRatio:  ${idoxPoissonRatio}" >> simulation.info 
        echo "   idoxWeibullModulus:  ${idoxWeibullModulus}" >> simulation.info 
        echo "   idoxTensileStrength_GPa:  ${idoxTensileStrength}" >> simulation.info 
        echo "   idoxCompressiveStrengthScale:  ${idoxCompressiveStrengthScale}" >> simulation.info 
        echo "Resolution parameters " >> simulation.info 
        echo "   partitionsPerDirection:  ${partitionsPerDirection}" >> simulation.info 
        echo "   cellsPerPartition:  ${cellsPerPartition}" >> simulation.info 
        echo "   particlesPerCellPerDirection:  2 (default)" >> simulation.info 
        echo "   domainLength_mm:  ${maxDomainLength}." >> simulation.info  
        echo "   materialPointParticleEdgeLength_mm:  ${formatted_value}"


        if [ serial_pfw ]
        then
                python3 particleFileWriter.py pfw_input_$runningFileName

        else 
                sbatch $pfwLaunchFileName
                echo "    Launched pfw in parallel on $n_cores cores ... "
        fi

fi

