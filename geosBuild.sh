#!/bin/bash

# ===========================================================================
# ||                                                                       ||
# ||               G E O S   B U I L D   S C R I P T                       ||
# ||                                                                       ||
# ||   This script clones the GEOS source code along with third party      ||
# ||   libraries that it requires, and then builds the GEOS executable.    ||
# ||                                                                       ||
# ||   Instructions:                                                       ||
# ||   -------------                                                       ||
# ||                                                                       ||
# ||   (1) Request interactive compute node session, i.e.,                 ||
# ||                                                                       ||
# ||       salloc -N1 -n36 -ppdebug                                        ||
# ||                                                                       ||
# ||   (2) Create and move to the workspace in which you want GEOS built,  ||
# ||       for example                                                     ||
# ||                                                                       ||
# ||       mkdir /usr/WS1/runnels1/geosBuild_<date>                        ||
# ||       cd    /usr/WS1/runnels1/geosBuild_<date>                        ||
# ||                                                                       ||
# ||   (3) Run this script by entering                                     ||
# ||                                                                       ||
# ||       salloc -N1 -ppdebug                                             ||
# ||                                                                       ||
# ||                                                                       ||
# ||   Notes:                                                              ||
# ||   ------                                                              ||
# ||                                                                       ||
# ||   (1)This script must be run on a compute node.  The git commands and ||
# ||      the ./scripts/setupLC-TPL.bash command will work on a front-end  ||
# ||      node.  But the scripts/config-build.py expects to be run on a    ||
# ||      compute node.                                                    ||
# ||                                                                       ||
# ||                                                                       ||
# ===========================================================================

# --------------------
# Defaults
# --------------------

system="dane"
mode="all"

# -----------------------
# Command-line arguments
# -----------------------

while getopts s:m:h flag
do
    case "${flag}" in
        s) system=${OPTARG};;
        h) echo "";
	   echo "";
	   echo "To run this script, enter";
	   echo "";
	   echo "./geosBuild.sh -s <system> -m <mode>";
	   echo "";
	   echo "   <system> is the name of the computer, e.g., dane";
	   echo "   <mode>   is optional and is 'all', 'clone', or 'build'.  It defaults to 'all'";
	   echo "";
	   echo "";
	   exit -1;;
        m) mode=${OPTARG};;
    esac
done

echo "";
echo "==============================================================";
echo "||                                                          ||";
echo "||            g e o s B u i l d . s h                       ||";
echo "||                                                          ||";
echo "|| This is the GEOS Workflows Build Script, geosBuild.sh    ||";
echo "||                                                          ||";
echo "===============================================geosBuild.sh===";
echo "";
 
if [ "$system" = "none" ]; then
    echo ""
    echo "Fatal Error"
    echo ""
    echo "You need to specify the system, e.g., ./geosBuild.sy -s dane."
    echo "Try ./geosBuild.sh -h for help."
    echo ""
    exit 1
fi

if [ "$system" != "dane" ]; then
    echo ""
    echo "Fatal Error"
    echo ""
    echo "dane is the only system currently supported."
    echo "You specified $system as the system."
    echo "Try ./geosBuild.sh -h for help."
    echo ""
    exit 1
fi

if [ "$system" = "dane" ]; then
    version="14"
fi


# ----------------------------------
# Set up directories and clone repos
# ----------------------------------


export GEOS_PARENT_DIR=$(pwd)

cd $GEOS_PARENT_DIR

if [ "$mode" = "clone" ] || [ "$mode" = "all" ]; then

    echo "";
    echo "==============================================================";
    echo "||                                                          ||";
    echo "||   Setting up directories and cloning repositories.       ||";
    echo "||                                                          ||";
    echo "===============================================geosBuild.sh===";
    echo "";

    rm -rf GEOS
    rm -rf thirdPartyLibs

    # Now let's clone some things:
   
#   git clone -b feature/crook5/mpm https://github.com/GEOS-DEV/GEOS.git --recursive
    git clone -b feature/appleton/psaap-mpm https://github.com/GEOS-DEV/GEOS.git --recursive
    git clone -b feature/appleton/psaap-mpm https://github.com/GEOS-DEV/thirdPartyLibs.git --recursive
fi


if [ "$mode" = "build" ] || [ "$mode" = "all" ]; then
    
    # ----------------------------------
    # Build third-party libraries
    # ----------------------------------
    
    echo "";
    echo "==============================================================";
    echo "||                                                          ||";
    echo "||   Building third-party libraries.                        ||";
    echo "||                                                          ||";
    echo "===============================================geosBuild.sh===";
    echo "";

    cd thirdPartyLibs

    # You can check out the usage for the bash script installer with ./scripts/setupLC-TPL.bash -h
    # 30-40 minutes required for this:

    ./scripts/setupLC-TPL.bash -geosPath "$GEOS_PARENT_DIR/GEOS" -bt "Release" -comp "clang@14"

    # ----------------------------------
    # Build GEOS
    # ----------------------------------

    echo "";
    echo "==============================================================";
    echo "||                                                          ||";
    echo "||   Building GEOS.                                         ||";
    echo "||                                                          ||";
    echo "===============================================geosBuild.sh===";
    echo "";


    cd ../GEOS

    module load clang
    python3 scripts/config-build.py -hc host-configs/LLNL/$system-clang@$version.cmake -bt Release
    cd build-$system-clang@$version-release
    make -j36 geosx

fi


echo "";
echo "==============================================================";
echo "||                                                          ||";
echo "||   Done                                                   ||";
echo "||                                                          ||";
echo "===============================================geosBuild.sh===";
echo "";

