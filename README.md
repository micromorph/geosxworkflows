# GEOS Workflows for PSAAP
In this repository, one will find Erik's take on how to get GEOS installed, working, and producing results for a number of PSAAP-relevant workflows. 
The goal here is to be an exhaustive overview of all the specific steps required to get a simulation working. 
All of these workflows are work by Jay Appleton, and any discussions or questions about the physics or GEOS in general should be directed his way. 
Any questions about the steps outlined here can be directed to Erik Jensen.

A quick note on "GEOS": "GEOS" denotes the totality of the scripts and codes required to produce results. 
"GEOS" is the specific name of the FEA code developed by Lawrence Livermore National Laboratory (LLNL), but we have co-opted the term here as a catch-all and will refer to this FEA code as "GEOS-FEA". 
"GEOS-MPM" is an addition to GEOS-FEA developed by Mike Homel and his team at LLNL for material point method (MPM) simulations using GEOS-FEA as the solver, and the "Particle File Writer" is a preprocessor for GEOS-MPM that creates initial configurations of MPM particles and GEOS-MPM input files.

Additional information on GEOS-FEA can be found here: [Documentation](https://geosx-geosx.readthedocs-hosted.com/en/latest/index.html).

## Installation Notes
Although a user will find installation instructions in the GEOS-FEA [Quick Start Guide](https://geosx-geosx.readthedocs-hosted.com/en/latest/docs/sphinx/QuickStart.html), this process includes having access to properly compiled and linked third party libraries or the patience to build them.
Instead we have streamlined this process for a single computer, Dane (LLNL), in the form of a series of helper files.
Included in the geosxworkflows root directory is a single helper script geosBuild.sh.
The user should navigate to the desired GEOS root directory, copy this script into that directory, and simply call  `./geosBuild.sh`.
This should build GEOS on Dane with Clang@14 compiler default selected.

1) Navigate to the directory into which you'd like to install GEOS, which will automatically be set to the variable `GEOS_PARENT_DIR` in the build script. Please note that the GEOS build will take up about 7GB of space.  Verify that there is appropriate storage in the `GEOS_PARENT_DIR` that you choose. 
```
cd $GEOS_PARENT_DIR
cp $PATH_TO_BUILD_SCRIPT/geosBuild.sh ./
./geosBuild.sh
``` 

8) A minimal viable Particle File Writer (PFW) is prepackaged with GEOS as a preprocess:
```
$GEOS_PARENT_DIR/GEOS/scripts/preProcessing/particleFileWriter/

```
The PFW comes with a few examples.

## Python
Python is used all over GEOS, and although most of the software can be run using the default version on Quartz, some of the workflows in this repository require some additional packages. Jay has successfully incorporated these packages directly into some of the workflows avoid this, but work is still being completed to update all of the workflows. 

If you come across a workflow that gives you strange python errors, Erik suggests setting up a local version of Python using virtual environments while you wait for the workflow to be updated. These instructions are useful in other contexts when using Quartz or other LC systems, so they'll be left here regardless of the status of workflow updates. For additional information, check out this guide: [Python on LC Systems](https://hpc.llnl.gov/software/development-environment-software/python)

1) Set up a new virtual environment to build your own version of python. Note that the command listed in the above instructions to do this doesn't work as `python` hasn't be properly linked to the current default version of python on Quartz (`python-3.10.8`). The argument `--system-site-packages` installs all the packages that come with the default version loaded upon login to Quartz, and the last argument of the command is where you'd like your version of python to be stored. You'll need to make the directory ahead of time, and as you can see, Erik decided to keep it simple and store it in his home directory under the creatively named sub-directory "python".
```
mkdir /g/g20/jensen58/python
cd /g/g20/jensen58/python/
/usr/tce/packages/python/python-3.10.8/bin/virtualenv --system-site-packages ./
```
2) You'll need to install the additional python packages listed below using the following general command replacing `<package>` with the name of the package you're installing making sure to use the version of `./pip` installed inside the virtual environment.
```
cd bin/
./pip install <package>
```
Installed Packages:
* imagecodecs - `./pip install imagecodecs` - Pre-processing
* NumPy-STL - `./pip install numpy-stl` - Particle File Writer
* NumPy - `./pip install numpy` - Post-processing 
* Matplotlib - `./pip install matplotlib` - Post-processing 
3) There are a lot of ways one can set up using this local version of python, but the simplest is to call it directly. Commands like the following work great.
```
/g/g20/jensen58/python/bin/python3 <script_to_run.py>
```

## ParaView
Post-processing of results is still a work-in-progress as the scripts to extract data from the output files need to be updated and there was a recent switch from VisIt to ParaView as the visualization software of choice. Erik is very bad at the overly cluttered and unintuitive software that is ParaView, so he'll need some help adding in suggestions for visualization. 

The following notes from LC will, however, get you up and running with ParaView and able to use a client-server approach to visualize GEOS results stored on LC systems locally: [ParaView Client-Server Mode](https://hpc.llnl.gov/running-paraview-client-server-mode). Erik found the instructions to be quite good. One could also use [RealVNC](https://hpc.llnl.gov/software/visualization-software/vnc-realvnc) to port the graphics of a full RedHat desktop to your local machine, although Erik found it gummed up his home directory on Quartz and has found the client-server approach to be just as quick. 

## Workflows
The following section contains a running list of all the currently developed and tested workflows. 

First though, a few quick notes on how GEOS works. 
1) A "runClean" Slurm script is used to start the simulation. Erik has found it easiest to use Quartz's batch scheduler (Slurm) to run simulations, but one could also manually check out a node if they wished. That node (and this script) will only run the Particle File Writer. 
2) The Particle File Writer generates all of the input files to GEOS-MPM, and, if asked in the Particle File Writer input script, will submit and run GEOS to the batch scheduler. This part is obscured from the user a bit, but all of the input files will end up in the output directory to look over later if desired. Also note that all Particle File Writer input scripts have the following form `pfw_input_<simulation_name>.py`, where `<simulation_name>` is a specific string that is looked for by the "runClean" script. More details later.

As a result, most of using GEOS is abstracted into manipulating the Particle File Writer input file and making sure all of the required information is moved around properly in the "runClean" script.

### Single Grain Unconfined Compression Between Two Platens (`TifStackGrain`)
This workflow simulates the compression of a single IDOX grain between two platens. In the [directory](https://gitlab.com/micromorph/geosxworkflows/-/tree/main/TifStackGrain?ref_type=heads), you will find files and a directory that require your attention prior to starting the simulation.
* `tifDirectory`: The directory in which the "runClean" script will look for the IDOX grain CT images (we call them TIF Stacks) that you'd like to smush. The user will need to manually upload the files, and Erik has tested the workflow with the TIF Stack of [this particle](https://micromorph.gitlab.io/projectwebsite/ExpDetailsForSample_Ptcl_IDOX2_01.html). Note that the TIF Stack must be "segmented"; the images must have been produced by [Segmentflow](https://gitlab.com/micromorph/segmentflow), which scrubs raw CT images into some useful for PSAAP simulations. The script was originally tested by Jay (and Jacob and Erik) using the version of the output in this Ex-1 Database entry: `Ptcl_IDOX2_1_Load0_3vss`.
* `getTifInfo.py`: A helper script that sifts through the TIF Stack and provides information that are then hardcoded into the PFW input file. Note that this script does not work with the default version of Python on Quartz and requires setting up a local version (see the Section "Python" above).
* `pfw_input_tifStackGrain.py`: The Particle File Writer input file. Additional details on how it works can be found in the script, and Erik plans to eventually highlight some specifics in this README. 
* `runClean_tifStackGrain.sh`: This is the "runClean" Slurm script.

A new user will need to add some additional information to the scripts before running. The information that needs to be added can be found at the line numbers provided below. The following is an example of the changes following the build Erik detailed in the installation notes above.

`getTifInfo.py`:
1) Line 18: Swap out the default path to the TIF Stack with the location of the TIF Stack you manually uploaded. Erik, for example, copied the files located in the PetaLibrary here: `[PetaLibrary]/SegmentflowOutput/IDOX_single_grains/Ptcl_IDOX2_1_Load0_3vss/PTCL_IDOX2_1_Load0_labeled_voxels` to a directory inside `tifDirectory` called `IDOX2_1_Load0_VSS3`. Note that the "/" at the end is important.
```
tifDirectory = '/usr/workspace/jensen58/geos/geosxworkflows/TifStackGrain/tifDirectory/IDOX2_1_Load0_VSS2/'
```
2) Run the script with a local version of Python. Following the instructions above, the command looks like this:
```
/g/g20/jensen58/python/bin/python3 getTifInfo.py
```

`pfw_input_tifStackGrain.py`:
1) Lines 26-40: Take the outputs from `getTifInfo.py` and copy/paste them into the block. Here are the outputs from `getTifInfo.py`
```
ct_numGrains =  255
         #  000.tif  to  259.tif
ct_minX =  485.05
ct_maxX =  1045.3100000000002
ct_minY =  147.15
ct_maxY =  671.44
ct_minZ =  0.0
ct_maxZ =  283.40000000000003
ct_voxelSize =  1.09
ct_ni =   1000
ct_nj =   1000
ct_nk =  260
```
and those outputs copied into the PFW input script 
```
scalingFactor = 1/1000    # ct info in um simulation in mm
ct_numGrains =  255
         #  000.tif  to  259.tif
ct_minX =  485.05 * scalingFactor
ct_maxX =  1045.3100000000002 * scalingFactor
ct_minY =  147.15 * scalingFactor
ct_maxY =  671.44 * scalingFactor
ct_voxelSize =  1.09 * scalingFactor
ct_minZ =  8*ct_voxelSize 
ct_maxZ =  283.40000000000003 * scalingFactor
ct_lengthZ = ct_maxZ - ct_minZ

ct_ni =   1000
ct_nj =   1000
ct_nk =  260
```
noting that the TIF Stack is in micrometers ($$\mu m$$) and the simulation is in millimeters ($$mm$$) requiring the use of the `scalingFactor`. If the units change, this parameter will need to be adjusted accordingly. Also note that Erik isn't sure what is going on with `ct_minZ`.

2) Line 13: Put in your LC username.
```
pfw["username"]='jensen58'
```
3) Line 14: Provide the PFW with the absolute path to your GEOS build (`GEOS_PARENT_DIR/GEOS/build-quartz-clang@14-release/bin/geosx` if you followed the above instructions).
```
pfw["geosPath"]='/usr/workspace/jensen58/geos/GEOS/build-quartz-clang@14-release/bin/geosx'
```
4) Line 15: Put in the PSAAP billing code.
```
pfw["mBank"] = 'uco'
```

`runClean_tifStackGrain.sh`:
1) Line 10: The "RunClean" will copy the TIF Stack to the simulation's working directory (see next input), so provide it's absolute location here. Note that you should not include a final "/" in the path.
```
tifDirLocation='/usr/workspace/jensen58/geos/geosxworkflows/TifStackGrain/tifDirectory/IDOX2_1_Load0_VSS3'
```
2) Line 11: Provide the directory into which you'd like to run GEOS and direct the outputs. Note that the script will create a sub-directory inside `runLocation` that will take the form `runLocation/<simulation_name>`. If that directory already exist - e.g., you've already run this simulation once, it will delete that directory and everything in it, and it will then remake the directory and start the simulation again. Be warned to save your previous results if you'd like to keep them!
```
runLocation='/p/lustre2/jensen58/geos'
```
3) Line 12: Put in the PSAAP billing code.
```
userBank="uco"
```
4) Line 18: In case you've moved this "runClean" script elsewhere, provide a hardcoded path to where your PFW input file (`pfw_input_tifStackGrain.py`) is located.
```
fileLocation='/usr/workspace/jensen58/geos/geosxworkflows/TifStackGrain'
```
5) Line 19: Provide the directory containing the Particle File Writer.
```
pfwFileLocation='/usr/workspace/jensen58/geos/pfw_geosx_temp'
```
6) Line 20: Provide the PFW with the absolute path to your GEOS build. This is here in case you'd like to print to the standard output the specific build you're using. To turn this feature on, switch `WRITE_RUN_INFO` on Line 14 to "true".
```
geosPath='/usr/workspace/jensen58/geos/GEOS/build-quartz-clang@14-release/bin/geosx'
```


<!-- The following has been depreciated to the best of Erik's knowledge -->
<!-- ### Single Grain Unconfined Compression Between Two Platens (`SingleSTL`)
This workflow simulates the compression of a single IDOX grain between two platens one will find the following four files. In the [directory](https://gitlab.com/micromorph/geosxworkflows/-/tree/main/SingleSTL?ref_type=heads), you will find two files and a directory that require your attention prior to starting the simulation.
* `stlDirectory`: The directory in which the "runClean" script will look for the IDOX grain triangular surface mesh (STL file) that you'd like to crush. The user will need to manually upload the file, but Erik can provide you with a low resolution example of [this particle](https://micromorph.gitlab.io/projectwebsite/ExpDetailsForSample_Ptcl_IDOX2_01.html) called `lowResIDOX2-1.stl` if you wish to test out the workflow.
* `pfw_input_stl.py`: The Particle File Writer input file. Additional details on how it works can be found in the script, and Erik plans to eventually highlight some specifics in this README. 
* `runClean_stl.sh`: This is the "runClean" Slurm script.

A new users will need to add some additional information to both scripts before running: "runClean" and the PFW input script. The information that needs to be added can be found at the top of the files (and if not, a line number is provided below). The following is an example of the changes following the build Erik detailed in the installation notes above.

`runClean_stl.sh`:
1) After placing the STL file you'd like to crush into `stlDirectory`, you'll need to provide the "runClean" script with its name.
```
stlFileName='lowResIDOX2-1.stl'
```
2) In case you've moved this "runClean" script elsewhere, provide a hard-coded path to where your PFW input file (`pfw_input_stl.py`) and `stlDirectory` are located. Those must be in the same directory.
```
fileLocation='/usr/workspace/jensen58/geos/geosxworkflows/SingleSTL'
```
3) Provide the directory containing your Particle File Writer build.
```
pfwFileLocation='/usr/workspace/jensen58/geos/pfw_geosx_temp'
```
4) Provide the directory into which you'd like to run GEOS and direct the outputs. Note that the script will create a sub-directory inside `runLocation` that will take the form `runLocation/<simulation_name>`. If that directory already exist - e.g., you've already run this simulation once, it will delete that directory and everything in it, and it will then remake the directory and start the simulation again. Be warned to save your previous results if you'd like to keep them!
```
runLocation='/p/lustre2/jensen58/geos'
```

`pfw_input_stl.py`:
1) Line 13: Provide your LC username.
```
pfw["username"]='jensen58'
```
2) Line 14: Provide the absolute path to your GEOS executable.
```
pfw["geosPath"]='/usr/workspace/jensen58/geos/GEOS/build-quartz-clang@14-release/bin/geosx'
```
3) Line 15: Set the LC billing code to the one for our Micromorph PSAAP Center (`uco`).
```
pfw["mBank"] = 'uco'
```
4) Line 26: Provide the name of the STL file you'd like to crush.
```
stlFileName = "lowResIDOX2-1.stl"
```

To run the simulation using Quartz's job scheduler, type the following command from `geosxworkflows/SingleSTL/` after making all the adjustments listed above.
```
sbatch runClean_stl.sh
``` -->