# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
import sys

pfw = {} 
pfw["runDebug"] = False

# Batch parameters for GEOS runs.  --------------------------------------------#
pfw["mBatch"]=True
pfw["mWallTime"]="06:30:00"
pfw["mSubmitJobs"]=True
pfw["autoRestart"] = False

# load pre-stress boundary cells

#------------------------------------------------------------------------------#
thisCPP = 16 			# general use cell per partition count
cylinderRadius = 2.5	# cylinder radius in mm
cylinderLength = 5.0
endTime = 250     		# let ring 
stepDisplacement = 0.0255 # micron step displacement on z+ boundary to initialize wave propagation

# Estane Matrix
estaneMSampleDensity = 1.935 #mg/mm3
estaneMYoungsModulus = 0.25 # GPa
estaneMPoissonRatio = 0.00 # 0.2 # 0.4 

thisRefine = 4  # domain is cube refine sets partition/direction
sampleBulk = estaneMYoungsModulus / (3*(1-2*estaneMPoissonRatio))
sampleShear = estaneMYoungsModulus / (2*(1+estaneMPoissonRatio))
sampleDensity = estaneMSampleDensity
pfw["cflFactor"]=0.125  

thisSolverProfiling=0   	# this option dumps profiling data to slurm-out.
#------------------------------------------------------------------------------#

# pfw flags
pfw["planeStrain"] = 0
pfw["useDamageAsSurfaceFlag"] = 1

# Domain ----------------------------------------------------------------------#
pfw["xpar"] = 1*(thisRefine)  	# grid partitions
pfw["ypar"] = 1*(thisRefine)
pfw["zpar"] = 1*(thisRefine)

cppZ=thisCPP
cppXY = cppZ+1
pfw["nI"]=2+pfw["xpar"]*cppXY  # grid cells in the x-direction
pfw["nJ"]=2+pfw["ypar"]*cppXY  # grid cells in the y-direction
pfw["nK"]=2+pfw["zpar"]*cppZ 	# grid cells in the z-direction
pfw["ppc"]=3   						# particles per cell in each direction

# Define all the geometric objects --------------------------------------------#
domainHeight = cylinderLength	+ 5.0*stepDisplacement # 1:1:1 ratio domain
domainWidth = pfw["nI"]/pfw["nK"]*domainHeight/2.0
endStretch = 1 - (stepDisplacement/domainHeight)

pfw["xmin"] = -0.5*domainWidth		# mm
pfw["xmax"] = 0.5*domainWidth		# mm
pfw["ymin"] = -0.5*domainWidth		# mm
pfw["ymax"] = 0.5*domainWidth		# mm
pfw["zmin"] = 0.0 					# mm
pfw["zmax"] = domainHeight  		# mm

dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# GEOSX MPM input parameters --------------------------------------------------#

pfw["objects"] = []

sample = geom.cylinder('cylinder',[0.0, 0.0, domainHeight - 2.0*cylinderRadius], [0.0, 0.0, domainHeight], 
	cylinderRadius, [0.0,0.0,0.0],0,0,0)

pfw["objects"] = pfw["objects"] + [sample]

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = endTime
pfw["plotInterval"] = endTime / (160.0)
pfw["restartInterval"] = 1e9
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = endTime/5000
pfw["boxAverageHistory"] = 0

pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'
pfw["fTable"]=[[0, 1, 1, 1],
[1e-6, 1, 1, endStretch]]

pfw["timeIntegrationOption"]="ExplicitDynamic" 
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 0, 0, 0, 0, 0, 2 ]
pfw["prescribedBcTable"] = 0
pfw["contactGapCorrection"] = 1
pfw["frictionCoefficient"] = 0.25

pfw["needsNeighborList"] = 0
pfw["neighborRadius"] = -1.01

pfw["materials"] = [ "LESample", "Sapphire" ]
pfw["materialPropertyString"]="""
<ElasticIsotropic
	name="LESample"
	defaultDensity="""+'"'+str(sampleDensity)+'"'+"""
	defaultBulkModulus="""+'"'+str(sampleBulk)+'"'+"""
	defaultShearModulus="""+'"'+str(sampleShear)+'"'+"""/>
<ElasticIsotropic
	name="Sapphire"
	defaultDensity="10.0"
	defaultBulkModulus="315"
	defaultShearModulus="172"/>
"""

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/36.)) 
#------------------------------------------------------------------------------#