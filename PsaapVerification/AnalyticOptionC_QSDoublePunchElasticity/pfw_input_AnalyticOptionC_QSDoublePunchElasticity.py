# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
import sys

pfw = {} 
pfw["runDebug"] = False

# Batch parameters for GEOS runs.  --------------------------------------------#
pfw["mBatch"]=True
pfw["mWallTime"]="18:30:00"
pfw["mSubmitJobs"]=True
pfw["autoRestart"] = True

#------------------------------------------------------------------------------#
# we will be making heavy use of quarter symmetry (x-y) and symmetry in z-base at 50mm height

thisRefine = 9  # domain is cube refine sets partition/direction
pfw["cflFactor"]=0.25  

thisCPP = 18 			# general use cell per partition count
endTime = 250     		# Quasistatic Hopefully
endStretch = 0.98

cylinderRadius = 50		# cylinder radius in mm
cylinderHeight = 100
cylinderDensity = 2.4   # mg/mm3
cylinderYoungsModulus = 37.4 # GPa
cylinderPoissonRatio  = 0.1 
cylinderBulkModulus = cylinderYoungsModulus / ( 3 * (1-2*cylinderPoissonRatio) )
cylinderShearModulus = cylinderYoungsModulus / ( 2 * (1+cylinderPoissonRatio) )

punchRadius = 2.5
punchHeight = 5.0
punchDensity = 10.0     # mg/mm3
punchBulkModulus = 315  # Sapphire like
punchShearModulus = 172  # Sapphire like

thisSolverProfiling=0   	# this option dumps profiling data to slurm-out.
#------------------------------------------------------------------------------#

# pfw flags
pfw["planeStrain"] = 0
pfw["useDamageAsSurfaceFlag"] = 1

# Domain ----------------------------------------------------------------------#
pfw["xpar"] = 1*(thisRefine)  	# grid partitions
pfw["ypar"] = 1*(thisRefine)
pfw["zpar"] = 1*(thisRefine)

cppZ=thisCPP
cppXY = cppZ 
pfw["nI"]=2+pfw["xpar"]*cppXY  # grid cells in the x-direction
pfw["nJ"]=2+pfw["ypar"]*cppXY  # grid cells in the y-direction
pfw["nK"]=2+pfw["zpar"]*cppZ 	# grid cells in the z-direction
pfw["ppc"]=2   						# particles per cell in each direction

# Define all the geometric objects --------------------------------------------#
domainHeight = cylinderRadius + punchHeight	# 1:1:1 ratio domain
domainWidth = cylinderRadius + punchHeight	

pfw["xmin"] = 0.0		# mm
pfw["xmax"] =  domainWidth		# mm
pfw["ymin"] = 0.0		# mm
pfw["ymax"] =  domainWidth		# mm
pfw["zmin"] = 0.0 					# mm
pfw["zmax"] =  domainHeight  		# mm

dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# GEOSX MPM input parameters --------------------------------------------------#

pfw["objects"] = []

cylinder = geom.cylinder('cylinder',[0.0, 0.0, -10.0], [0.0, 0.0, cylinderRadius], 
	cylinderRadius, [0.0,0.0,0.0],0,0,0)
punch = geom.cylinder('cylinder',[0.0, 0.0, cylinderRadius], [0.0, 0.0, domainHeight+10.0], 
	punchRadius, [0.0,0.0,0.0],1,0,0)

pfw["objects"] = [cylinder, punch]

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = endTime
pfw["plotInterval"] = endTime / (100.0)
pfw["restartInterval"] = 1e9
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = endTime/3500
pfw["boxAverageHistory"] = 0

pfw["fTable"]=[[0, 1, 1, 1],
[endTime, 1, 1, endStretch]]

pfw["timeIntegrationOption"]="ExplicitDynamic" 
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 1, 1, 1, 1, 1, 2 ]
pfw["prescribedBcTable"] = 0
pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'
pfw["contactGapCorrection"] = 1
pfw["frictionCoefficient"] = 0.25

pfw["needsNeighborList"] = 0
pfw["neighborRadius"] = -1.01

pfw["materials"] = [ "Cylinder", "Punch" ]
pfw["materialPropertyString"]="""
<ElasticIsotropic
	name="Cylinder"
	defaultDensity="""+'"'+str(cylinderDensity)+'"'+"""
	defaultBulkModulus="""+'"'+str(cylinderBulkModulus)+'"'+"""
	defaultShearModulus="""+'"'+str(cylinderShearModulus)+'"'+"""/>
<ElasticIsotropic
	name="Punch"
	defaultDensity="""+'"'+str(punchDensity)+'"'+"""
	defaultBulkModulus="""+'"'+str(punchBulkModulus)+'"'+"""
	defaultShearModulus="""+'"'+str(punchShearModulus)+'"'+"""/>
"""

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/36.)) 
#------------------------------------------------------------------------------#