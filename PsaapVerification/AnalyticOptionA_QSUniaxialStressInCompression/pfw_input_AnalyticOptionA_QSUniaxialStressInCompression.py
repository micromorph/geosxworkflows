# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
import sys

pfw = {} 
pfw["runDebug"] = False

# Batch parameters for GEOS runs.  --------------------------------------------#
pfw["mBatch"]=True
pfw["mWallTime"]="02:00:00"
pfw["mSubmitJobs"] = False
pfw["autoRestart"] = False

#------------------------------------------------------------------------------#
thisCPP = 16 			# general use cell per partition count
cylinderRadius = 2.5	# cylinder radius in mm
endTime = 500     		# Quasistatic Hopefully
endStretch = 0.99

# Estane Matrix
estaneMSampleDensity = 1.935 #mg/mm3
estaneMYoungsModulus = 0.25 # GPa
estaneMPoissonRatio = 0.2   # 0.4

thisRefine = 1  # domain is cube refine sets partition/direction
sampleBulk = estaneMYoungsModulus / (3*(1-2*estaneMPoissonRatio))
sampleShear = estaneMYoungsModulus / (2*(1+estaneMPoissonRatio))
sampleDensity = estaneMSampleDensity
pfw["cflFactor"]=0.25  

thisSolverProfiling=0   	# this option dumps profiling data to slurm-out.
#------------------------------------------------------------------------------#

# pfw flags
pfw["planeStrain"] = 0
pfw["useDamageAsSurfaceFlag"] = 1

# Domain ----------------------------------------------------------------------#
pfw["xpar"] = 1*(thisRefine)  	# grid partitions
pfw["ypar"] = 1*(thisRefine)
pfw["zpar"] = 1*(thisRefine)

cppZ=thisCPP
cppXY = cppZ+1
pfw["nI"]=2+pfw["xpar"]*cppXY  # grid cells in the x-direction
pfw["nJ"]=2+pfw["ypar"]*cppXY  # grid cells in the y-direction
pfw["nK"]=2+pfw["zpar"]*cppZ 	# grid cells in the z-direction
pfw["ppc"]=2   						# particles per cell in each direction

# Define all the geometric objects --------------------------------------------#
domainHeight = 1.0 * (2.0 * cylinderRadius)	# 1:1:1 ratio domain
domainWidth = pfw["nI"]/pfw["nK"]*domainHeight

pfw["xmin"] = -0.5*domainWidth		# mm
pfw["xmax"] =  0.5*domainWidth		# mm
pfw["ymin"] = -0.5*domainWidth		# mm
pfw["ymax"] =  0.5*domainWidth		# mm
pfw["zmin"] = 0.0 					# mm
pfw["zmax"] = domainHeight  		# mm

dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# GEOSX MPM input parameters --------------------------------------------------#

pfw["objects"] = []

sample = geom.cylinder(name='cylinder',x1=[0.0, 0.0, 0.0], x2=[0.0, 0.0, domainHeight], 
	r=cylinderRadius, v=[0.0,0.0,0.0],mat=0,group=0)

pfw["objects"] = pfw["objects"] + [sample]

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = endTime
pfw["plotInterval"] = endTime / (100.0)
pfw["restartInterval"] = 1e9
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = endTime/2600
pfw["boxAverageHistory"] = 0

pfw["fTable"]=[[0, 1, 1, 1],
[endTime, 1, 1, endStretch]]

pfw["timeIntegrationOption"]="ExplicitDynamic" 
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 0, 0, 0, 0, 2, 2 ]
pfw["prescribedBcTable"] = 0
pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'
pfw["contactGapCorrection"] = 'Simple'
pfw["frictionCoefficient"] = 0.25
pfw["updateMethod"]="XPIC"
pfw["updateOrder"]=2

pfw["needsNeighborList"] = 0
pfw["neighborRadius"] = -1.01

pfw["materials"] = [ "LESample" ]
pfw["materialPropertyString"]="""
<ElasticIsotropic
	name="LESample"
	defaultDensity="""+'"'+str(sampleDensity)+'"'+"""
	defaultBulkModulus="""+'"'+str(sampleBulk)+'"'+"""
	defaultShearModulus="""+'"'+str(sampleShear)+'"'+"""/>
"""

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
#------------------------------------------------------------------------------#