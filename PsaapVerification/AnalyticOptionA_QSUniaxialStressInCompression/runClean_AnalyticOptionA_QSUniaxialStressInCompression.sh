#!/bin/bash
#SBATCH -t 0:45:00
#SBATCH -N 1
#SBATCH -A uco

# Set fileName=xxx (no spaces), where the input file is pfw_input_xxx.py 

fileName=AnalyticOptionA_QSUniaxialStressInCompression
runningFileName=AnalyticOptionA_QSUniaxialStressInCompression # this sets the run location directory name
runLocation='__PATH_TO_RUNLOCATION__'
userBank="__USER_BANK__"
userName="$(whoami)"
WRITE_RUN_INFO=false  # Should run info of successful run be appended to readme

# ==========================================================================================================================================
# This should be the location of the input file and anything else you need to copy over:
fileLocation='__PATH_TO_pfw_input_AnalyticOptionA_QSUniaxialStressInCompression.py__'
pfwFileLocation='__PATH_TO_PFW_AND_GEOMCLASSES__'
geosPath='__PATH_TO_GEOS__'

#Write and possibly run files
if [ -n "$fileName" ]
then
	echo "Running job: "$runningFileName
	rm -rf $runLocation/$runningFileName/                                       # delete old results for the same fileName!!!
	mkdir -p $runLocation/$runningFileName/                                     # create the run/output directory

	cp $fileLocation/pfw_input_$fileName.py $runLocation/$runningFileName       # copy the input file
	cp $pfwFileLocation/particleFileWriter.py $runLocation/$runningFileName     # copy the preprocessor
	cp $pfwFileLocation/pfw_check.py $runLocation/$runningFileName              # copy the autoRestart script
	cp $pfwFileLocation/pfw_geometryObjects.py $runLocation/$runningFileName    # copy the geometry object functions
	cp -r $pfwFileLocation/thirdPartyLibraries/ $runLocation/$runningFileName   # move tpls to run location

	cd $runLocation/$runningFileName                                            # move to the run location
    # Create userDefs file
    echo "# -*- coding: utf-8 -*-" > userDefs_$(whoami).py
    echo "geosPath=\"${geosPath}\"" >> userDefs_$(whoami).py
    echo "testRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
    echo "defaultRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
    echo "defaultBank=\"${userBank}\"" >> userDefs_$(whoami).py

	python3 particleFileWriter.py pfw_input_$fileName.py                        # launch the VML
	# srun -n36 
fi