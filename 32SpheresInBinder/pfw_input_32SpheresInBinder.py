# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
from sklearn.neighbors import KDTree          # nearest neighbor search with KDTree

# START!

pfw = {} 
pfw["runDebug"] = False

# Batch parameters for GEOS runs.  --------------------------------------------#

pfw["mBatch"]=True
pfw["mWallTime"]="08:30:00"
pfw["mSubmitJobs"] = True
pfw["autoRestart"] = True

# stuff
#------------------------------------------------------------------------------#
thisSolverProfiling = 0		# 0/1 no/yes dump to console

thisRefine = __THIS_REFINE__  		# The domain is thisRefine partitions per direction
thisCPP = 14 			# each partition is thisCPP cells per direction 

endCompressionTime = (1.5/5) * 1000 * (np.pi/10.0) / 2     # 50 pi us  Note: i messed this up originally and ran for 1/5 us
endTime = endCompressionTime + 10  # us
endStretch = 0.9

# IDOX: E=22GPa,ν=0.25,ρ=2.001mg/mm3
# Estane: E=0.002GPa,ν=0.499,ρ=1.190mg/mm3
# Estane Matrix: E=0.25GPa,ν=0.4,ρ=1.935mg/mm3

#  Need to address binder stretching maybe use 3 ppc and definitely look at the polymer model and cady's paper

# Idox 
# sampleDensity = 2.001 #mg/mm3
idoxYoungsModulus = 22.0  # GPa
idoxPoissonRatio = 0.25

# # Estane
# sampleDensity = 1.190 #mg/mm3
estaneMYoungsModulus = 0.250 # GPa
estaneMPoissonRatio = 0.4

# sampleBulk = youngsModulus / (3*(1-2*poissonRatio))
# sampleShear = youngsModulus / (2*(1+poissonRatio))

idoxDensity = 2.001				# Idox density
idoxBulkModulus = idoxYoungsModulus / (3*(1-2*idoxPoissonRatio))			# idox bulk
idoxShearModulus = idoxYoungsModulus / (2*(1+idoxPoissonRatio))		# idox shear
idoxTensileStrength = 0.25		# idox GPA
idoxCompressiveStrengthScale = 4.0  # Mike's 8ish-to-1 rule of thumb
idoxCompressiveStrength = idoxCompressiveStrengthScale*idoxTensileStrength #temp

thisWeibullSeed = 1
idoxWeibullModulus = 2.5        # __WEIBULL_MODULUS__
estaneMWeibullModulus = 6.0
thisWeibullVolume = 1.0e-5      # __WEIBULL_VOLUME__ Picked to give good results

estaneMDensity = 1.935				# estane density
estaneMBulkModulus = estaneMYoungsModulus / (3*(1-2*estaneMPoissonRatio))			# estane bulk
estaneMShearModulus = estaneMYoungsModulus / (2*(1+estaneMPoissonRatio))		# estane shear
estaneMTensileStrength = 0.2		# estane GPA
estaneMCompressiveStrengthScale = 8.0  # Mike's 8ish-to-1 rule of thumb
estaneMCompressiveStrength = estaneMCompressiveStrengthScale * estaneMTensileStrength #temporarily
#------------------------------------------------------------------------------#

# pfw flags -------------------------------------------------------------------#
pfw["planeStrain"] = 0
pfw["useDamageAsSurfaceFlag"] = 1

# Domain ----------------------------------------------------------------------#
pfw["xpar"] = thisRefine  	# grid partitions
pfw["ypar"] = thisRefine
pfw["zpar"] = thisRefine

cpp=thisCPP
pfw["nI"]=2+pfw["xpar"]*cpp  		# grid cells in the x-direction
pfw["nJ"]=2+pfw["ypar"]*cpp  		# grid cells in the y-direction
pfw["nK"]=2+pfw["zpar"]*cpp 		# grid cells in the z-direction
pfw["ppc"]=2   						# particles per cell in each direction

# Define all the geometric objects --------------------------------------------#
domainHeight = 1.332 			# mm   To help maintain uniform grid sizes
xyPadding = 0.0

pfw["xmin"] = -0.5*domainHeight - xyPadding		# mm
pfw["xmax"] =  0.5*domainHeight + xyPadding		# mm
pfw["ymin"] = -0.5*domainHeight - xyPadding		# mm
pfw["ymax"] =  0.5*domainHeight + xyPadding		# mm
pfw["zmin"] =  0.0 								# mm
pfw["zmax"] =  domainHeight  					# mm

dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# GEOSX MPM input parameters --------------------------------------------------#

pfw["objects"] = []

# specify an array with all objects to be included, order matters for 
# overlapping objects, the first one listed will be assigned at each point
# "fill" must be last on the list.

binder = geom.cylinder('binder',[0.0,0.0,pfw["zmin"]-2.0*dz],
	[0.0,0.0,pfw["zmax"]+2.0*dz],0.5,[0.0,0.0,0.0],0,0,0)


xShift = -0.5
yShift = -0.5
zShift =  0.0


# class voronoiWeibullBox:
dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]
ppvc = 10  
flawSize = np.min([dx,dy,dz])*ppvc     
vMin=dx*dy*dz

# class sphere:
#   'Geometry object for creating a sphere defined by center and radius'
#   def __init__(self,name,x0,r,v,mat,group,damage):

centersRadiiArray=np.array([[0.168+xShift, 0.542+yShift, 0.374, 0.165 ],
	[0.5+xShift, 0.231+yShift, 0.164, 0.164],
	[0.422+xShift, 0.323+yShift, 0.686, 0.163],
	[0.454+xShift, 0.834+yShift, 1.07, 0.162],
	[0.283+xShift, 0.239+yShift, 0.405, 0.16],
	[0.457+xShift, 0.563+yShift, 0.898, 0.16],
	[0.541+xShift, 0.607+yShift, 0.594, 0.159],
	[0.589+xShift, 0.512+yShift, 0.295, 0.158],
	[0.545+xShift, 0.545+yShift, 1.2, 0.157],
	[0.338+xShift, 0.803+yShift, 0.452, 0.156],   
	[0.746+xShift, 0.74+yShift, 0.995, 0.155],
	[0.595+xShift, 0.168+yShift, 0.902, 0.155],
	[0.846+xShift, 0.511+yShift, 0.473, 0.154],   
	[0.842+xShift, 0.444+yShift, 0.88, 0.153],
	[0.762+xShift, 0.271+yShift, 0.643, 0.152],
	[0.199+xShift, 0.622+yShift, 0.677, 0.151],   
	[0.849+xShift, 0.515+yShift, 1.17, 0.15],
	[0.39+xShift, 0.316+yShift, 1.07, 0.149],
	[0.754+xShift, 0.743+yShift, 0.148, 0.148],   
	[0.148+xShift, 0.499+yShift, 1.12, 0.147],
	[0.761+xShift, 0.262+yShift, 0.328, 0.146],
	[0.741+xShift, 0.241+yShift, 1.15, 0.145],    
	[0.854+xShift, 0.468+yShift, 0.145, 0.145],
	[0.179+xShift, 0.346+yShift, 0.871, 0.144],
	[0.354+xShift, 0.621+yShift, 0.143, 0.143],   
	[0.808+xShift, 0.682+yShift, 0.71, 0.142],
	[0.495+xShift, 0.859+yShift, 0.206, 0.141],
	[0.436+xShift, 0.854+yShift, 0.727, 0.14],    
	[0.676+xShift, 0.815+yShift, 0.415, 0.139], 
	[0.555+xShift, 0.143+yShift, 0.484, 0.138],
	[0.186+xShift, 0.331+yShift, 0.137, 0.137], 
	[0.21+xShift,  0.718+yShift, 0.948, 0.136] ]) #32

sphere1 = geom.sphere('sphere1', centersRadiiArray[1-1,:3], centersRadiiArray[1-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere2 = geom.sphere('sphere2', centersRadiiArray[2-1,:3], centersRadiiArray[2-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere3 = geom.sphere('sphere3', centersRadiiArray[3-1,:3], centersRadiiArray[3-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere4 = geom.sphere('sphere4', centersRadiiArray[4-1,:3], centersRadiiArray[4-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere5 = geom.sphere('sphere5', centersRadiiArray[5-1,:3], centersRadiiArray[5-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere6 = geom.sphere('sphere6', centersRadiiArray[6-1,:3], centersRadiiArray[6-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere7 = geom.sphere('sphere7', centersRadiiArray[7-1,:3], centersRadiiArray[7-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere8 = geom.sphere('sphere8', centersRadiiArray[8-1,:3], centersRadiiArray[8-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere9 = geom.sphere('sphere9', centersRadiiArray[9-1,:3], centersRadiiArray[9-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere10 = geom.sphere('sphere10', centersRadiiArray[10-1,:3], centersRadiiArray[10-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere11 = geom.sphere('sphere11', centersRadiiArray[11-1,:3], centersRadiiArray[11-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere12 = geom.sphere('sphere12', centersRadiiArray[12-1,:3], centersRadiiArray[12-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere13 = geom.sphere('sphere13', centersRadiiArray[13-1,:3], centersRadiiArray[13-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere14 = geom.sphere('sphere14', centersRadiiArray[14-1,:3], centersRadiiArray[14-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere15 = geom.sphere('sphere15', centersRadiiArray[15-1,:3], centersRadiiArray[15-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere16 = geom.sphere('sphere16', centersRadiiArray[16-1,:3], centersRadiiArray[16-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere17 = geom.sphere('sphere17', centersRadiiArray[17-1,:3], centersRadiiArray[17-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere18 = geom.sphere('sphere18', centersRadiiArray[18-1,:3], centersRadiiArray[18-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere19 = geom.sphere('sphere19', centersRadiiArray[19-1,:3], centersRadiiArray[19-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere20 = geom.sphere('sphere20', centersRadiiArray[20-1,:3], centersRadiiArray[20-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere21 = geom.sphere('sphere21', centersRadiiArray[21-1,:3], centersRadiiArray[21-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere22 = geom.sphere('sphere22', centersRadiiArray[22-1,:3], centersRadiiArray[22-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere23 = geom.sphere('sphere23', centersRadiiArray[23-1,:3], centersRadiiArray[23-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere24 = geom.sphere('sphere24', centersRadiiArray[24-1,:3], centersRadiiArray[24-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere25 = geom.sphere('sphere25', centersRadiiArray[25-1,:3], centersRadiiArray[25-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere26 = geom.sphere('sphere26', centersRadiiArray[26-1,:3], centersRadiiArray[26-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere27 = geom.sphere('sphere27', centersRadiiArray[27-1,:3], centersRadiiArray[27-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere28 = geom.sphere('sphere28', centersRadiiArray[28-1,:3], centersRadiiArray[28-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere29 = geom.sphere('sphere29', centersRadiiArray[29-1,:3], centersRadiiArray[29-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere30 = geom.sphere('sphere30', centersRadiiArray[30-1,:3], centersRadiiArray[30-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)
sphere31 = geom.sphere('sphere31', centersRadiiArray[31-1,:3], centersRadiiArray[31-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)	
sphere32 = geom.sphere('sphere32', centersRadiiArray[32-1,:3], centersRadiiArray[32-1,3], 
	[0.0,0.0,0.0], 1, 0, 0)

vwSphere1 = geom.voronoiWeibullBox('vw',sphere1,
  centersRadiiArray[1-1,:3] - (centersRadiiArray[1-1,3] + flawSize),
  centersRadiiArray[1-1,:3] + (centersRadiiArray[1-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere2 = geom.voronoiWeibullBox('vw',sphere2,
  centersRadiiArray[2-1,:3] - (centersRadiiArray[2-1,3] + flawSize),
  centersRadiiArray[2-1,:3] + (centersRadiiArray[2-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere3 = geom.voronoiWeibullBox('vw',sphere3,
  centersRadiiArray[3-1,:3] - (centersRadiiArray[3-1,3] + flawSize),
  centersRadiiArray[3-1,:3] + (centersRadiiArray[3-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere4 = geom.voronoiWeibullBox('vw',sphere4,
  centersRadiiArray[4-1,:3] - (centersRadiiArray[4-1,3] + flawSize),
  centersRadiiArray[4-1,:3] + (centersRadiiArray[4-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere5 = geom.voronoiWeibullBox('vw',sphere5,
  centersRadiiArray[5-1,:3] - (centersRadiiArray[5-1,3] + flawSize),
  centersRadiiArray[5-1,:3] + (centersRadiiArray[5-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere6 = geom.voronoiWeibullBox('vw',sphere6,
  centersRadiiArray[6-1,:3] - (centersRadiiArray[6-1,3] + flawSize),
  centersRadiiArray[6-1,:3] + (centersRadiiArray[6-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere7 = geom.voronoiWeibullBox('vw',sphere7,
  centersRadiiArray[7-1,:3] - (centersRadiiArray[7-1,3] + flawSize),
  centersRadiiArray[7-1,:3] + (centersRadiiArray[7-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere8 = geom.voronoiWeibullBox('vw',sphere8,
  centersRadiiArray[8-1,:3] - (centersRadiiArray[8-1,3] + flawSize),
  centersRadiiArray[8-1,:3] + (centersRadiiArray[8-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere9 = geom.voronoiWeibullBox('vw',sphere9,
  centersRadiiArray[9-1,:3] - (centersRadiiArray[9-1,3] + flawSize),
  centersRadiiArray[9-1,:3] + (centersRadiiArray[9-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere10 = geom.voronoiWeibullBox('vw',sphere10,
  centersRadiiArray[10-1,:3] - (centersRadiiArray[10-1,3] + flawSize),
  centersRadiiArray[10-1,:3] + (centersRadiiArray[10-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere11 = geom.voronoiWeibullBox('vw',sphere11,
  centersRadiiArray[11-1,:3] - (centersRadiiArray[11-1,3] + flawSize),
  centersRadiiArray[11-1,:3] + (centersRadiiArray[11-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere12 = geom.voronoiWeibullBox('vw',sphere12,
  centersRadiiArray[12-1,:3] - (centersRadiiArray[12-1,3] + flawSize),
  centersRadiiArray[12-1,:3] + (centersRadiiArray[12-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere13 = geom.voronoiWeibullBox('vw',sphere13,
  centersRadiiArray[13-1,:3] - (centersRadiiArray[13-1,3] + flawSize),
  centersRadiiArray[13-1,:3] + (centersRadiiArray[13-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere14 = geom.voronoiWeibullBox('vw',sphere14,
  centersRadiiArray[14-1,:3] - (centersRadiiArray[14-1,3] + flawSize),
  centersRadiiArray[14-1,:3] + (centersRadiiArray[14-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere15 = geom.voronoiWeibullBox('vw',sphere15,
  centersRadiiArray[15-1,:3] - (centersRadiiArray[15-1,3] + flawSize),
  centersRadiiArray[15-1,:3] + (centersRadiiArray[15-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere16 = geom.voronoiWeibullBox('vw',sphere16,
  centersRadiiArray[16-1,:3] - (centersRadiiArray[16-1,3] + flawSize),
  centersRadiiArray[16-1,:3] + (centersRadiiArray[16-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere17 = geom.voronoiWeibullBox('vw',sphere17,
  centersRadiiArray[17-1,:3] - (centersRadiiArray[17-1,3] + flawSize),
  centersRadiiArray[17-1,:3] + (centersRadiiArray[17-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere18 = geom.voronoiWeibullBox('vw',sphere18,
  centersRadiiArray[18-1,:3] - (centersRadiiArray[18-1,3] + flawSize),
  centersRadiiArray[18-1,:3] + (centersRadiiArray[18-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere19 = geom.voronoiWeibullBox('vw',sphere19,
  centersRadiiArray[19-1,:3] - (centersRadiiArray[19-1,3] + flawSize),
  centersRadiiArray[19-1,:3] + (centersRadiiArray[19-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere20 = geom.voronoiWeibullBox('vw',sphere20,
  centersRadiiArray[20-1,:3] - (centersRadiiArray[20-1,3] + flawSize),
  centersRadiiArray[20-1,:3] + (centersRadiiArray[20-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere21 = geom.voronoiWeibullBox('vw',sphere21,
  centersRadiiArray[21-1,:3] - (centersRadiiArray[21-1,3] + flawSize),
  centersRadiiArray[21-1,:3] + (centersRadiiArray[21-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere22 = geom.voronoiWeibullBox('vw',sphere2,
  centersRadiiArray[22-1,:3] - (centersRadiiArray[22-1,3] + flawSize),
  centersRadiiArray[22-1,:3] + (centersRadiiArray[22-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere23 = geom.voronoiWeibullBox('vw',sphere23,
  centersRadiiArray[23-1,:3] - (centersRadiiArray[23-1,3] + flawSize),
  centersRadiiArray[23-1,:3] + (centersRadiiArray[23-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere24 = geom.voronoiWeibullBox('vw',sphere24,
  centersRadiiArray[24-1,:3] - (centersRadiiArray[24-1,3] + flawSize),
  centersRadiiArray[24-1,:3] + (centersRadiiArray[24-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere25 = geom.voronoiWeibullBox('vw',sphere25,
  centersRadiiArray[25-1,:3] - (centersRadiiArray[25-1,3] + flawSize),
  centersRadiiArray[25-1,:3] + (centersRadiiArray[25-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere26 = geom.voronoiWeibullBox('vw',sphere26,
  centersRadiiArray[26-1,:3] - (centersRadiiArray[26-1,3] + flawSize),
  centersRadiiArray[26-1,:3] + (centersRadiiArray[26-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere27 = geom.voronoiWeibullBox('vw',sphere27,
  centersRadiiArray[27-1,:3] - (centersRadiiArray[27-1,3] + flawSize),
  centersRadiiArray[27-1,:3] + (centersRadiiArray[27-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere28 = geom.voronoiWeibullBox('vw',sphere28,
  centersRadiiArray[28-1,:3] - (centersRadiiArray[28-1,3] + flawSize),
  centersRadiiArray[28-1,:3] + (centersRadiiArray[28-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere29 = geom.voronoiWeibullBox('vw',sphere29,
  centersRadiiArray[29-1,:3] - (centersRadiiArray[29-1,3] + flawSize),
  centersRadiiArray[29-1,:3] + (centersRadiiArray[29-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere30 = geom.voronoiWeibullBox('vw',sphere30,
  centersRadiiArray[30-1,:3] - (centersRadiiArray[30-1,3] + flawSize),
  centersRadiiArray[30-1,:3] + (centersRadiiArray[30-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere31 = geom.voronoiWeibullBox('vw',sphere31,
  centersRadiiArray[31-1,:3] - (centersRadiiArray[31-1,3] + flawSize),
  centersRadiiArray[31-1,:3] + (centersRadiiArray[31-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)
vwSphere32 = geom.voronoiWeibullBox('vw',sphere32,
  centersRadiiArray[32-1,:3] - (centersRadiiArray[32-1,3] + flawSize),
  centersRadiiArray[32-1,:3] + (centersRadiiArray[32-1,3] + flawSize),
  flawSize, thisWeibullVolume, idoxWeibullModulus, thisWeibullSeed, vMin)

# binder = geom.cylinder('binder',[0.0,0.0,pfw["zmin"]-2.0*dz],
# 	[0.0,0.0,pfw["zmax"]+2.0*dz],0.5,[0.0,0.0,0.0],0,0,0)

vwBinder = geom.voronoiWeibullBox('vw', binder, 
	[pfw["xmin"]-2.0*dz, pfw["ymin"]-2.0*dz, pfw["zmin"]-2.0*dz], 
	[pfw["xmax"]+2.0*dz, pfw["ymax"]+2.0*dz, pfw["zmax"]+2.0*dz], 
	flawSize, thisWeibullVolume, estaneMWeibullModulus, thisWeibullSeed, vMin)

# objects=[binder]
# pfw["objects"] = pfw["objects"] + [sphere1, sphere2, sphere3, sphere4, 
# 	sphere5, sphere6, sphere7, sphere8,	sphere9, sphere10, sphere11, sphere12, 
# 	sphere13, sphere14, sphere15, sphere16, sphere17, sphere18, sphere19, 
# 	sphere20, sphere21, sphere22, sphere23, sphere24, sphere26, sphere27, 
# 	sphere28, sphere29, sphere30, sphere31, sphere32, binder]

pfw["objects"] = [vwSphere1, vwSphere2, vwSphere3, vwSphere4, vwSphere5,
	vwSphere6, vwSphere7, vwSphere8, vwSphere9, vwSphere10, vwSphere11, vwSphere12,
	vwSphere13, vwSphere14, vwSphere15, vwSphere16, vwSphere17, vwSphere18, vwSphere19,
	vwSphere20, vwSphere21, vwSphere22, vwSphere23, vwSphere24, vwSphere25, vwSphere26,
	vwSphere27, vwSphere28, vwSphere29, vwSphere30, vwSphere31, vwSphere32, vwBinder]

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = endTime
pfw["plotInterval"] = endTime / 4.0 #240.0
pfw["restartInterval"] = endTime / 4
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = endTime / 3600.0
pfw["boxAverageHistory"] = 0

pfw["timeIntegrationOption"]="ExplicitDynamic"
pfw["cflFactor"]=0.25   
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 2, 2, 2, 2, 2, 2 ]
pfw["prescribedBcTable"] = 0
pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'

# FTable:
pfw["fTable"]=[[-pfw["endTime"], 1, 1, 2 - endStretch],
# [0, 1, 1, 1],
[pfw["endTime"], 1, 1, endStretch]]

pfw["damageFieldPartitioning"] = 1
pfw["needsNeighborList"] = 1
pfw["neighborRadius"] = -1.01
pfw["useDamageAsSurfaceFlag"] = 1
pfw["contactGapCorrection"] = 0
pfw["frictionCoefficient"] = 0.25

pfw["materials"] = [ "Binder", "Idox" ]
pfw["materialPropertyString"]="""
<CeramicDamage
	name="Binder"
	defaultDensity="""+"\""+str(estaneMDensity)+"\""+"""
	defaultBulkModulus="""+"\""+str(estaneMBulkModulus)+"\""+"""
	defaultShearModulus="""+"\""+str(estaneMShearModulus)+"\""+"""
	tensileStrength="""+'"'+str(estaneMTensileStrength)+'"'+"""
	compressiveStrength="""+'"'+str(estaneMCompressiveStrength)+'"'+"""
	maximumStrength="""+'"'+str(1.25 * estaneMCompressiveStrength)+'"'+"""
	crackSpeed="0.8"
	/>
<CeramicDamage
	name="Idox"
	defaultDensity="""+"\""+str(idoxDensity)+"\""+"""
	defaultBulkModulus="""+"\""+str(idoxBulkModulus)+"\""+"""
	defaultShearModulus="""+"\""+str(idoxShearModulus)+"\""+"""
	tensileStrength="""+'"'+str(idoxTensileStrength)+'"'+"""
	compressiveStrength="""+'"'+str(idoxCompressiveStrength)+'"'+"""
	maximumStrength="""+'"'+str(1.5 * idoxCompressiveStrength)+'"'+"""
	crackSpeed="1.8"
	/>
"""

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
#------------------------------------------------------------------------------#
