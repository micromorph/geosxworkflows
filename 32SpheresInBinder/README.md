# GEOS(X) Simulation: 32 Spheres in Idealized Binder

This simulation establishes a GEOS(X) link between yade sphere dumps stored as csv files
and a GEOS(X)-MPM simulation by manually defining the spheres and binder. 

Questions can be directed toward Jay Appleton (jay.appleton@colorado.edu).

## The Quick How To:

The the simulation is defined by a particle file writer input script with key values:
- value 1;
- value 2;
- value 3;
- value 4.

The simulation is initialized by running the particle file writer (not included).
Fill in values in runClean and userDefs. 
 
================================================================================ 

### Run Date and time: 2024-02-21 13:50:23 PST 
### Bash Version: GNU bash, version 4.4.20(1)-release (x86_64-redhat-linux-gnu) 
	
    GEOS Git Info:
    - URL: https://github.com/GEOS-DEV/GEOS.git
    - Branch: feature/appleton/psaap-mpm
    - Commit: 20ae7db3235c8d32634f4eac9f2d95c1ac9981cb 
	
    PFW Git Info:
    - URL: ssh://git@czgitlab.llnl.gov:7999/homel1/pfw_geosx_temp.git
    - Branch: feature/appleton/psaap-mpm
    - Commit: 2e610daec60b3e7dab9f50b96eb62e335f60644d 
================================================================================
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 

