# -*- coding: utf-8 -*-
import pfw_geometryObjects as geom   # this contains all the geometry object functions for pfw
import numpy as np                   # math stuff
import sys

pfw = {} 
pfw["runDebug"] = True

# Batch parameters for GEOS runs.  --------------------------------------------#
pfw["mBatch"]=True
pfw["mWallTime"]="00:30:00"
pfw["mSubmitJobs"]=True
pfw["autoRestart"] = False

#------------------------------------------------------------------------------#
thisCPP = __CELLS_PER_PARTITION_BASE__  # cell per partition count
cylinderRadius = __CYLINDER_RADIUS__    # 2.5 # cylinder radius in mm
endTime = __END_TIME__                  # 100     		
endStretch = __END_STRETCH__            # 0.975 

# sample material parameters
sampleDensity = __MAT_DENSITY__         # 1.935 #mg/mm3
sampleYoungsModulus = __MAT_E__         # 0.25 # GPa
samplePoissonRatio = __MAT_NU__         # 0.2   # 0.4

thisRefine = __PARTITIONS_PER_DIRECTION__  # domain partitioning 
sampleBulk = sampleYoungsModulus / (3*(1-2*samplePoissonRatio))
sampleShear = sampleYoungsModulus / (2*(1+samplePoissonRatio))

pfw["cflFactor"]=0.25  

# Domain ----------------------------------------------------------------------#
pfw["xpar"] = 1*(thisRefine)  	# grid partitions
pfw["ypar"] = 1*(thisRefine)
pfw["zpar"] = 1*(thisRefine)

cppZ=thisCPP
cppXY = cppZ+0#1
pfw["nI"]=pfw["xpar"]*cppXY  # grid cells in the x-direction
pfw["nJ"]=pfw["ypar"]*cppXY  # grid cells in the y-direction
pfw["nK"]=pfw["zpar"]*cppZ 	# grid cells in the z-direction
pfw["ppc"]=2   						# particles per cell in each direction

# Define all the geometric objects --------------------------------------------#
domainHeight = 1.0 * (2.0 * cylinderRadius)	# 1:1:1 ratio domain
domainWidth = pfw["nI"]/pfw["nK"]*domainHeight

pfw["xmin"] = -0.5*domainWidth		# mm
pfw["xmax"] =  0.5*domainWidth		# mm
pfw["ymin"] = -0.5*domainWidth		# mm
pfw["ymax"] =  0.5*domainWidth		# mm
pfw["zmin"] = 0.0 					# mm
pfw["zmax"] = domainHeight  		# mm

dx = (pfw["xmax"]-pfw["xmin"])/(pfw["nI"]-2)/pfw["ppc"]
dy = (pfw["ymax"]-pfw["ymin"])/(pfw["nJ"]-2)/pfw["ppc"]
dz = (pfw["zmax"]-pfw["zmin"])/(pfw["nK"]-2)/pfw["ppc"]

# GEOSX MPM input parameters --------------------------------------------------#

pfw["objects"] = []

sample = geom.cylinder('cylinder',[0.0, 0.0, 0.0], [0.0, 0.0, domainHeight], 
	cylinderRadius, v=[0.0,0.0,0.0],mat=0,group=0)

# sample = geom.box('cylinder',[0.0-cylinderRadius, 0.0-cylinderRadius, 0.0], 
# 	[0.0+cylinderRadius, 0.0+cylinderRadius, domainHeight], 
# 	v=[0.0,0.0,0.0],mat=0,group=0)
# basePlaten = geom.box('base', [-100, -100, -100], [100, 100, basePlatenDepth], 
# 	v=[0,0,0], mat=0, group=0)

pfw["objects"] = pfw["objects"] + [sample]

# Solver Values ----------------------------------------------------------------


# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
#------------------------------------------------------------------------------#

# Solver Values ----------------------------------------------------------------
pfw["endTime"] = endTime
pfw["plotInterval"] = endTime / 12.0
pfw["restartInterval"] = endTime / 1
pfw["reactionHistory"] = 1
pfw["reactionWriteInterval"] = endTime / 500.0
pfw["boxAverageHistory"] = 0

pfw["updateMethod"]="XPIC"
pfw["updateOrder"]=2

pfw["timeIntegrationOption"]="ExplicitDynamic"
pfw["cflFactor"]=0.25   
pfw["initialDt"]=1e-16

pfw["cpdiDomainScaling"] = 1
pfw["bodyForce"] = [ 0, 0, 0 ]
pfw["boundaryConditionTypes"] = [ 0, 0, 0, 0, 2, 2 ]
pfw["prescribedBcTable"] = 0
pfw["prescribedBoundaryFTable"] = 1  
pfw["fTableInterpType"] = 'Cosine'

pfw["fTable"]=[[0, 1, 1, 1],
[endTime, 1, 1, endStretch]]

pfw["damageFieldPartitioning"] = 0
pfw["needsNeighborList"] = 0
pfw["neighborRadius"] = -1.01
pfw["useDamageAsSurfaceFlag"] = 0
pfw["frictionCoefficient"] = 0.05

pfw["materials"] = [ "LESample" ]
pfw["materialPropertyString"]="""
<ElasticIsotropic
	name="LESample"
	defaultDensity="""+'"'+str(sampleDensity)+'"'+"""
	defaultBulkModulus="""+'"'+str(sampleBulk)+'"'+"""
	defaultShearModulus="""+'"'+str(sampleShear)+'"'+"""/>
"""

# Define pfw["particleFileFields"]
#------------------------------------------------------------------------------#
pfw["particleFileFields"] = ["Velocity", "MaterialType", "ContactGroup", 
							 "SurfaceFlag", "StrengthScale", "RVector"]

#------------------------------------------------------------------------------#
pfw["plottableFields"] =["particleReferencePosition", "particleDensity",
 "particleMaterialType", "particleCenter", "particleVelocity", "particleVolume",
 "particleID", "particleAcceleration", "particleStress", "particleDamage"]

# Process values:
#------------------------------------------------------------------------------#
pfw["mCores"]=pfw["xpar"]*pfw["ypar"]*pfw["zpar"]
pfw["mNodes"]=int(np.ceil(float(pfw["mCores"])/112.)) 
#------------------------------------------------------------------------------#
