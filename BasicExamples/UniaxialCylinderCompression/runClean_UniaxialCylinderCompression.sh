#!/bin/bash
#SBATCH -t 0:45:00
#SBATCH -N 1
#SBATCH -A uco

# Set fileName=xxx (no spaces), where the input file is pfw_input_xxx.py 

fileName=UniaxialCylinderCompression
runningFileName=UniaxialCylinderCompression # this sets the run location directory name
runLocation='__PATH_TO_RUNLOCATION__'
userBank="__USER_BANK__"
userName="$(whoami)"

# Simulation Parameters with some default values 
cellsPerPartition='8'           # cells per partition along z axis
partitionsPerDirection='1'      # partitions per direction in x,y, and z
cylinderRadius='2.5'            # 5mm is the default cylinder diameter and height
simulationEndTime='100.0'       # end time in microseconds
simulationEndStretch='0.975'    # cosine interpolated end stretch 
materialDensity='1.935'         # material density in mg/mm3
materialYoungsModulus='0.25'    # material youngs modulus in GPa
materialPoissonsRatio='0.20'    # material poissons ratio

# ==========================================================================================================================================
# This should be the location of the input file and anything else you need to copy over:
fileLocation='__PATH_TO_pfw_input_UniaxialCylinderCompression.py__'
pfwFileLocation='__PATH_TO_PFW_AND_GEOMCLASSES__'
geosPath='__PATH_TO_GEOS__'

#Write and possibly run files
if [ -n "$fileName" ]
then
	echo "Running job: "$runningFileName
	rm -rf $runLocation/$runningFileName/                                       # delete old results for the same fileName!!!
	mkdir -p $runLocation/$runningFileName/                                     # create the run/output directory

	cp $fileLocation/pfw_input_$fileName.py $runLocation/$runningFileName       # copy the input file
	cp $pfwFileLocation/particleFileWriter.py $runLocation/$runningFileName     # copy the preprocessor
	cp $pfwFileLocation/pfw_check.py $runLocation/$runningFileName              # copy the autoRestart script
	cp $pfwFileLocation/pfw_geometryObjects.py $runLocation/$runningFileName    # copy the geometry object functions
	# cp -r $pfwFileLocation/thirdPartyLibraries/ $runLocation/$runningFileName   # move tpls to run location

	cd $runLocation/$runningFileName                                            # move to the run location
    # Create userDefs file
    echo "# -*- coding: utf-8 -*-" > userDefs_$(whoami).py
    echo "geosPath=\"${geosPath}\"" >> userDefs_$(whoami).py
    echo "testRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
    echo "defaultRunDirectory=\"${runLocation}\"" >> userDefs_$(whoami).py
    echo "defaultBank=\"${userBank}\"" >> userDefs_$(whoami).py

    # swap out parameters
    sed -i "s#__CELLS_PER_PARTITION_BASE__#${cellsPerPartition}#g" pfw_input_$runningFileName.py   
    sed -i "s#__CYLINDER_RADIUS__#${cylinderRadius}#g" pfw_input_$runningFileName.py   
    sed -i "s#__PARTITIONS_PER_DIRECTION__#${partitionsPerDirection}#g" pfw_input_$runningFileName.py  
    sed -i "s#__END_TIME__#${simulationEndTime}#g" pfw_input_$runningFileName.py
    sed -i "s#__END_STRETCH__#${simulationEndStretch}#g" pfw_input_$runningFileName.py
    sed -i "s#__MAT_DENSITY__#${materialDensity}#g" pfw_input_$runningFileName.py
    sed -i "s#__MAT_E__#${materialYoungsModulus}#g" pfw_input_$runningFileName.py
    sed -i "s#__MAT_NU__#${materialPoissonsRatio}#g" pfw_input_$runningFileName.py

	python3 particleFileWriter.py pfw_input_$fileName.py                        # launch the VML
	# srun -n36 
fi